# H3 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.



### Übung 1 - Radio- und Checkboxfelder

**(zusammen mit den Kompetenzen abzugeben)**


a) Erstellen sie ein Formular zu den folgenden Fragen:

- Welches ist ihr aktuelles Lieblingsspiel (Computerspiele) (10 Spiele)
- Welche der folgenden Spiele spielen sie regelmässig? (10 Spiele)

b) Fügen sie Escape-Optionen hinzu. Escape-Optionen können angeklickt werden, wenn keine der Standard-Optionen passen, z.B.

- Möglichkeit für "Ich spiele keine Spiele"
- Möglichkeit für "Andere: ". Achtung: hier ist ein zusätzliche Textfeld notwendig. Wie würden sie dieses darstellen?

**Schicken Sie das Formular zurück auf sich selbst, ohne serverseitigen Code**



### Übung 2 - Demographische Datenerhebung

**(zusammen mit den Kompetenzen abzugeben)**


fieldset und optgroup selber herausfinden und anwenden und Beispiel erstellen

**Schicken Sie das Formular zurück auf sich selbst, ohne serverseitigen Code**



### Übung 3 - Bilder hochladen

**(zusammen mit den Kompetenzen abzugeben)**


Erstellen Sie ein Formular mit welchem Sie Bilder hochladen können. **Sie sollen das Formular abschicken können, müssen aber kein Backend dazu schreiben!**


