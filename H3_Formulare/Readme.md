![TBZ-Logo](../x_gitressources/tbz_logo.png)

# HTML-Formulare (H3)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann die HTML-Elemente in einem Formular benennen.
- Ich kann kann ein einfaches Formular erstellen.
- Ich kann eine geeignete Request-Methode wählen.
- Ich kann über ein Formular eine Datei hochladen.
- Ich kann versteckte Form-Elemente korrekt anwenden.



## Form-Tag

Ein Formular wird immer durch den *form*-Tag repräsentiert. Nur Formular-Elemente innerhalb des Tags, werden automatisch übermittelt, wenn das Formular abgeschickt wird.

``<form action="" method="" autocomplete="">....</form>``

Im Prinzip sind keine der Attribute notwendig und haben Standardwerte, aber typischerweise finden sie folgende:
| Attribute | Beschreibung |
| ---------------- | ------------------------------------------------------------------------|
| action | Den Pfad auf dem Server auf den dieses Formular geschickt wird, z.B. ``action="/process/form.html"``. Dies kann auch eine absolute URL sein, mit Domain-Angabe, also ``action="https://www.example.com/process/form.html"``. |
| method | Die Methode, die verwendet wird um das Formular zu übermitteln, z.B. ``method="post"``. Gültige Werte sind nur *GET* und *POST*. <br/> Achtung: "post" geht in github.io nicht. Dafür brauchen Sie einen "richtigen" Webspace (siehe T3) |
| autocomplete | Definiert, ob Formular-Felder automatisch ausgefüllt werden, basierend auf ihreren früheren Angaben, z.B. ``autocomplete="on"``. Gültige Werte sind nur *ON* und *OFF*. Dies ist eine Browser-Funktion und das Verhalten kann sich unterscheiden pro Browser. |

### Methoden und Datenübertragung

Wir haben früher bereits gelernt, dass die Datenübertragung mit den verschiedenen Methoden (GET, POST, DELETE, etc.) unterschiedliche Semantik beinhaltet. Aber wie sieht das nun mit einem Formular aus? Die Übertragung funktioniert genau gleich, aber ein Formular kann nur die folgenden beiden Methoden verwenden:

- POST: Daten werden im Nachrichten-Body übertragen. Die Formular-Inhalte werden als Schlüssel-Werte-Paare in den Body geschrieben.
- GET: Daten werden in der URL übertragen. Die Formular-Inhalte werden als Schlüssel-Werte-Paare in der URL übertragen.

Erstellen sie eine lokale Datei mit [diesem Inhalt](https://codepen.io/nuy/pen/XWzzMoR). Halten sie die Developer-Toolbar offen und untersuchen sie den Body der Pakete und die URL, wenn sie das Formular abschicken.

**Allgemein**: Alle Beispiele liegen auf [CodePen](https://codepen.io/collection/oEYgeo). Wenn sie aber ein Formular abschicken möchten, erstellen sie besser eine lokale Datei

**Achtung**: Wenn sie zuerst das GET-Formular übermitteln, sehen sie die Daten bereits in der URL. Diese werden nicht entfernt für die Übertragen mit post. Sie haben so anschliessen die Daten doppelt, einmal in der URL und einmal im Body. Entfernen sie die URL-Parameter zuerst, bevor die das POST-Formular übermitteln.

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_form.asp>



## Einfache Felder

Im Beispiel mit den Formularen haben sie bereits gesehen, dass Formular-Felder verwendet wurden. Es gibt verschiedene Typen der einfachen Felder, wobei der Typ "text" ist, wenn nichts anderes angegeben wurde. Der Browser stellt die verschiedenen Typen unterschiedlich dar.

~~~html
<form>
    <input type="" name="" value="" />
</form>
~~~

Auf [dieser CodePen-Seite](https://codepen.io/nuy/pen/abVVJMV) werden alle Formular-Feld Typen beschrieben. Im Beispiel werden bereits feste Werte eingetragen, die so übermittelt werden können. Anstelle der Werte kann man auch **Platzhalter** einsetzen, wobei dann die Felder leer bleiben, aber dem Benutzer Hinweise auf den Inhalt gegeben werden. [Beispiele mit Platzhalter](https://codepen.io/nuy/pen/PoOOpvr).

**Achtung**: Viele der Typen sind neu(er) und finden im Web oft keine Verwendung. Die Typen wurden eingeführt, damit sich die Felder visuell unterscheiden und die Validierung typen-gerecht Informationen zeigt (dazu mehr später). Da die client-seitige Validierung einfach umgangen werden kann, sind die Input-Typen nicht sehr populär.

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_input.asp>
- <https://www.w3schools.com/tags/att_input_type.asp>



## Textbereich

Der Textbereich wird verwendet, wenn mehr Text geschrieben wird, als auf einer Zeile, so dass kein Standard-Input-Feld verwendet werden kann.

~~~html
<form>
    <textarea name="beschreibung" placeholder="Schreiben sie hier einen Kommentar"></textarea>
</form>
~~~

[Beispiel Kontaktformular mit Textarea](https://codepen.io/nuy/pen/YzEEVzd)

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_textarea.asp>



## Auswahl-, Radio- und Checkboxen

Wir haben bereits gesehen wie man Radio- und Checkboxen erstellt. Wir gehen hier aber vertiefter auf diese Elemente ein, da sie oft Verwendung finden.

**Checkboxen** kommen oft in Gruppen vor, können aber auch alleine für sich stehen, abhängig von der Anwendung. Checkbox-Felder die gruppiert werden, müssen das Attribut **name** teilen, also den gleichen Wert haben. Mit dem Attribut **checked** kann gesteuert werden, welche Felder standardmässig bereits angeklickt sind.

~~~html
<form>
    Welche Speisen mögen sie:<br />
    <input type="checkbox" name="food" value="Pasta" checked /><br />
    <input type="checkbox" name="food" value="Reis" checked /><br />
    <input type="checkbox" name="food" value="Kartoffeln" /><br />
    <br />
    Sind sie sicher?<br />
    <input type="checkbox" name="sicher" value="1" />
</form>
~~~

**Radioboxen** kommen (fast) immer in Gruppen vor, es steht sehr selten ein Radio-Input alleine im Formular. Auch hier teilen sich die Input-Felder das Attribut **name**. Wenn sie ein Radio-Feld anklicken, werden automatisch alle anderen Radio-Felder mit dem gleichen Namen zurückgesetzt und sind nicht mehr angeklickt. Mit dem Attribut **checked** kann gesteuert werden, welches Feld standardmässig bereits angeklickt ist.

~~~html
<form>
    Welches ist ihre Lieblingsspeise:<br />
    <input type="radio" name="food" value="Pasta" /><br />
    <input type="radio" name="food" value="Reis" checked /><br />
    <input type="radio" name="food" value="Kartoffeln" />
</form>
~~~

**Select-Felder** sind eine Alternative zu Radio- und Checkbox-Feldern. Der Tag ist **select**, welche **option**-Tags enthalten. Dieser Tag wird als ein Dropdown oder Multiselect (Einfachauswahl oder Mehrfachauswahl) dargestellt. Von der Funktion her, ist er gleichbedeutend wie gruppierte Radioboxen oder gruppierte Checkboxen. Abhängig von den Einstellungen kann ein oder mehrere Werte ausgewählt werden. Dieser Tag wird verwendet, wenn aus **vielen Einträgen ausgewählt werden muss**, so dass eine Checkbox- oder Radio-Liste zu unübersichtlich wird.

~~~html
<form>
    Welche Speisen mögen sie:<br />
    <select name="food1" multiple>
        <option value="Pasta" selected>Pasta</option>
        <option value="Reis" selected>Reis</option>
        <option value="Kartoffeln">Kartoffeln</option>
    </select>
    <br />
    <br />
    Welches ist ihre Lieblingsspeise:<br />
    <select name="food2">
        <option value="Pasta">Pasta</option>
        <option value="Reis" selected>Reis</option>
        <option value="Kartoffeln">Kartoffeln</option>
    </select>
</form>
~~~

[Beispiel radio, checkbox, select](https://codepen.io/nuy/pen/abVVWzq)

[Beispiel Kontaktformular Auswahlfeldern](https://codepen.io/nuy/pen/jOaamPr)

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_input.asp>
- <https://www.w3schools.com/tags/tag_select.asp>
- <https://www.w3schools.com/tags/tag_option.asp>



## Label

Der *label*-Tag enthält die Beschriftung zu einem Form-Feld, so dass klar ist, welche Eingabe erwartet wird. Immer öfters wird der Label weggelassen zugunsten des *Placeholder*-Attributes. Eine Kombination ist aber durchaus hilfreich. Im Label kann die Beschriftung und der Platzhalter ein Beispiel enthalten.

Wenn man auf den Label klickt, kriegt das Form-Feld automatisch den Fokus. Dies ist speziell hilfreich bei kleinen Feldern wie Checkbox und Radioboxen.

~~~html
<form>
    <label for="name">Name: </label><input id="name" type="text" name="name" />
</form>
~~~

Beachten sie, dass das *for*-Attribut den gleichen Inhalt enthalten muss, wie das *id*-Attribut des Form-Feldes.

Der *label*-Tag kann auch zur Gestaltung des Formulars verwendet werden. In früheren Beispielen wurden Tabellen verwendet. Im folgenden Beispiel werden nur *label*-Tags und *formular*-Tags verwendet. Während das HTML einfacher erscheint, ist das zugehörigen CSS komplizierter. Gearbeitet wurde hier mit floating-Containers. Vorstellen kann man sich auch das *flexbox*- oder *grid*-System.

**Hinweis**: Verzichten sie auf Tabellen-Layouts und verwenden sie korrekte Labels, Containers.

[Beispiel Kontaktformular mit Labels (ohne Tabelle)](https://codepen.io/nuy/pen/oNooWXa)

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_label.asp>



## Validierung

Validierung in HTML (client-seitig) ist möglich aber unsicher! Jeder kann mit der Developer-Toolbar die Attribute ändern und so die Validierung umgehen und das Formular trotzdem abschicken. Erinnern sie sich immer die **Validierungsregeln** bei echten Live-Systemen:

1. Clientseitige Validierung ist notwendig für die Benutzerfreundlichkeit
2. Serverseitige Validieerung ist notwendig für die Integrität und Sicherheit der Applikation

Wir arbeiten in diesem Modul aber nur clientseitig und können somit auch nur entsprechende Funktionalität verwenden.

Validierung geschieht automatisch während des Abschickens eines Formulares und hängt von den verwendeten Attributen ab, die folgend vorgestellt werden.

| Tag | Attribut | Beschreibung |
| ------------ | --------------- | ------------------------------------------------------|
| form | novalidate | Ist ein binäres Attribut, welches die Validierung im gesamten Formular deaktiviert.<br />``<form novalidate>....</form>`` |
| input, textarea | required | ist ein binäres Attribut, welches eine Eingabe erzwingt<br />``<input type="text" name="name" required />`` |
| input | type | Abhängig davon was im *type*-Attribut steht, wird entsprechend validiert. Im folgenden Beispiel müssen sie eine E-Mail Adresse eingeben.<br />``<input type="email" name="email" required />`` |
| input | min | Das *min*-Attribut ist gültig, falls sie beim *type*-Attribute "number" verwenden.<br />``<input type="number" name="age" min="10" />`` |
| input | max | Das *max*-Attribut ist gültig, falls sie beim *type*-Attribute "number" verwenden.<br />``<input type="number" name="age" max="99" />`` |
| input | pattern | Das *pattern*-Attribut ist gültig für verschiedene Typen. Erwartet wird eine [*Regular Expression*](https://www.w3schools.com/jsref/jsref_obj_regexp.asp). Dies allerdings sprengt den Inhalt des Moduls<br />``<input type="url" name="website" pattern="https?://.+" />`` |

[Beispiel Kontaktformular mit Validierung](https://codepen.io/nuy/pen/dyZZWYQ)

[Beispiel Registrierung mit Validierung](https://codepen.io/nuy/pen/MWOOmKm)

### Für Profis: Eigene Fehlermeldungen

Eigene Fehlermeldungen zu schreiben ist möglich, aber nur mit Hilfe von JavaScript und dem Event-System von HTML. Unter den folgenden Links finden sie Beispiele. Beachten sie, dass die Radioboxen nicht behandelt werden, weil es zwei Input-Felder sind und die Status-Änderung auf beiden Feldern überprüft werden müsste. Zusätzliches JavaScript wäre notwendig.

[Beispiel Kontaktformular mit Validierung und eigenen Fehlermeldungen](https://codepen.io/nuy/pen/BammRKy)

[Beispiel Registrierung mit Validierung und eigenen Fehlermeldungen](https://codepen.io/nuy/pen/rNYYmeY)

Quellen:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/tags/tag_input.asp>
- <https://www.w3schools.com/tags/tag_textarea.asp>
- <https://www.w3schools.com/jsref/jsref_obj_regexp.asp>



## Dateien übertragen

Wir haben bei den [einfachen Feldern](./#einfache-felder) gesehen, dass der Typ "file" existiert. Mit diesem Input-Typ können sie Dateien an den Server übertragen. Es sind ein paar Erweiterungen am Formular und Input-Feld notwendig.

~~~html
<form method="post" enctype="multipart/form-data">
    Datei
    <input type="file" name="file" 
        accept="image/*,video/*"
        multiple
        required
    />
    <input type="submit" name="submit" value="Abschicken" />
</form>
~~~

Die folgenden Attribute wurden hinzugefügt

| Tag | Attribut | Beschreibung |
| ------- | -------------- | ------------------------------------------------------------------- |
| form | enctype | Der *enctype* ändert den Content-Type mit dem die Daten des Formulars übertragen werden. Den Content-Type kennen sie bereits aus dem [früheren Thema "Protokoll"](../T2_Protokoll/Readme.md#inhaltstypen-content-type). Per Default wird der Content-Type  *x-www-form-urlencoded* verwendet. Wenn sie auf *multipart/form-data* wechseln, ist es ihnen möglich Dateien zu übertragen. |
| input | accept | Mit diesem Attribut steuern sie, welche Inhalte man im Explorer auswählen kann. Akzeptiert wird eine Komma-separierte Liste mit [Content-Types (oder Mime-Types)](https://w3schools.sinsixx.com/media/media_mimeref.asp.htm)<br />**Achtung**: Diese Einschränkung ist nicht validiert und kann einfach überschrieben werden. Sie müssen immer server-seitig den Datei-Typ überprüfen.  |
| input | multiple | Wenn sie dieses binäre Attribut setzen, können sie mehrere Dateien auswählen, die übertragen werden. |
| input | required | Die required-Validierung funktioniert auch für Inputs des Typs *file* |

[Beispiel Datei Mehrfachauswahl](https://codepen.io/nuy/pen/vYWWmGM)

Quellen:

- <https://www.w3schools.com/tags/att_form_enctype.asp>
- <https://w3schools.sinsixx.com/media/media_mimeref.asp.htm>
- <https://www.w3schools.com/tags/att_input_type.asp>
- <https://www.w3schools.com/tags/att_input_type_file.asp>



## Checkpoints

- [ ] Ich kann die HTML-Elemente in einem Formular benennen.
- [ ] Ich kann kann ein einfaches Formular erstellen.
- [ ] Ich kann eine geeignete Request-Methode wählen.
- [ ] Ich kann über ein Formular eine Datei hochladen.
- [ ] Ich kann versteckte Form-Elemente korrekt anwenden.



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 15.05.2022, Yves Nussle
