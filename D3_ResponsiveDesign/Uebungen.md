# D3 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.



### Übung 1 - Pocket Gallery

Erstellen Sie eine Webseite für eine kleine Bildergalerie bestehend aus **sechs eigenen Fotos**. Wählen Sie einen Titel für Ihre Galerie und bauen Sie diesen mit  `<h1>`  als Überschrift der Seite ein. Positionieren Sie die Fotos mit einem Grid so, dass diese auf einem Smartphone in einer Spalte, auf einem Tablet in zwei Spalten und auf einem Desktop in 3 Spalten dargestellt werden. Das Grid soll responsive sein und die Darstellung automatisch an das entsprechende Ausgabegerät anpassen. Wählen sie den Mobile-First-Ansatz und legen Sie die notwendigen Breakpoints fest. Auch der Titel sollte sich an die Bildschirmgrösse anpassen. Testen Sie die Seite auf allen Geräten.

Erklären Sie in Prosa wie Ihr Responsive Design aufgebaut ist und wie es funktioniert. Geben Sie an, welches Ihre Breakpoint sind und wieso und mit welchen Mitteln Sie diese erreicht haben.



### Übung 2 - Mobile Shop

Erstellen Sie eine Shop-Seite mit **sechs verwandten Produkten** und stellen Sie diese in einer Produktübersicht dar. Fügen Sie jedem Produkt einen Titel, einen Preis und einen Kaufen-Button hinzu. Wählen Sie den Mobile-First-Ansatz und setzen Sie den Shop als Responsive Design um. Legen Sie 3 Breakpoints fest und optimieren Sie die Darstellung für Smartphone, Tablet und Desktop. Testen Sie die Seite auf allen Geräten.

Erklären Sie in Prosa wie Ihr Responsive Design aufgebaut ist und wie es funktioniert. Geben Sie an, welches Ihre Breakpoint sind und wieso und mit welchen Mitteln Sie diese erreicht haben.
