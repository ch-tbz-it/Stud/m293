![TBZ Logo](../x_gitressources/tbz_logo.png)

# Responsive Design (D3)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

1. Ich kann erläutern wozu Responsive Design dient.
2. Ich kann erklären wie eine CSS Media Query funktioniert.
3. Ich kann geeignete Breakpoints für verschiedene Endgeräte setzen.
4. Ich kann das responsive Verhalten eines Layouts steuern.
5. Ich kann eine responsive Webseite gemäss Vorgaben umsetzen.



## Einführung

In [CSS-Layouts](../D2_LayoutStyles) haben wir uns angeschaut, wie wir Elemente auf einer Webseite anordnen können und wir haben verschiedene Layout-Methoden, wie Flexbox und Grid kennengelernt. In diesem Teil schauen wir uns an, wie ein Layout für unterschiedliche Bildschirmgrössen entwickelt werden kann.

### Was ist Responsive Design?

Zu Beginn des Webdesigns wurden Webseiten für **eine** bestimmte Bildschirmgrösse entwickelt. Durch die Entwicklung des Flachbildschirms wurden immer höhere Auflösungen möglich. Doch während die Bilschirme immer grösser wurden, kamen mit dem Smartphone und dem Tablet noch kleinere Bildschirmgrössen hinzu. Heute entwickeln wir eine Webseite für **unterschiedliche** Bildschirmgrössen, damit sie auf allen Endgeräten ein gutes Nutzererlebnis bietet.

Responsive Design bedeutet, dass sich ein Webseiten-Layout automatisch an das jeweilige Ausgabemedium anpasst. Dies tut es natürlich nicht von selbst, sondern muss in der Webentwicklung entsprechend umgesetzt werden. Dank einem einfachen Prinzip können wir mit **CSS** aus einem statischen Layout ein dynamisches Layout erstellen.

### Wie funktioniert Responsive Design?

Ein Responsive Design ist im Grunde eine Sammlung von Regeln, die in Form von **Media Queries** verschiedene Varianten eines Designs bereit halten. Ist der Bildschirm eines Ausgabemediums z.B. besonders klein, erkennt der Browser die Regel für kleine Bildschirme wendet das Design für kleine Bildschirme an. Dieses Prinzip wird in der Praxis über mehrere Abstufungen mit Hilfe von **Breakpoints** realisiert, um eine Webseite für jede Bildschirmgrösse optimal nutzbar zu machen.



## HTML Viewport Meta-Tag

Im Browser wird meistens nur ein kleiner Bereich einer Webseite dargestellt. Dieser sichtbare Bereich wird als Viewport bezeichnet. Ist eine Webseite grösser als der Viewport, werden vom Browser Scrollbars eingeblendet, um durch den gesamten Inhalt zu scrollen. Alternativ kann eine Webseite skaliert werden, damit ein bestimmtes Element oder die ganze Seite im Viewport sichtbar ist. Dies ist natürlich nicht das beste Nutzererlebnis, doch eine wichtige Voraussetzung, um unabhängig von der Bildschirmauflösung alle Inhalte einer Webseite zugänglich zu machen.

Mit der Breite, Höhe und der Skalierung verfügt der Viewport über drei Eigenschaften, die das Grundverhalten bestimmen. Der Viewport orientiert sich entweder an einer bestimmten Breite oder an einer bestimmten Höhe und die Skalierung definiert wie stark eine Webseite vergrössert oder verkleinert werden soll.

Um eine responsive Webseite zu erstellen, fügen Sie im HTML-Head den folgenden Meta-Tag hinzu:

~~~html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
~~~

### Ausrichtung

**width**

Durch die Viewport-Eigenschaft `width=device-width` verhält sich der Viewport responsiv zur **Bildschirmbreite** des jeweiligen Ausgabemediums. Die meisten Webseiten sind vertikal aufgebaut. Inhalte die ausserhalb des Viewports liegen, können von oben nach unten gescrollt werden. Wird die Breite z.B. mit  `width=960` als eine feste Pixelbreite definiert, ist kein responsives Design möglich. Der Viewport hätte dann unabhängig vom Ausgabemedium immer die gleiche Breite.

**height**

Durch die Viewport-Eigenschaft `height=device-height` verhält sich der Viewport responsiv zur **Bildschirmhöhe** des jeweiligen Ausgabemediums. Dies ist  nur sinnvoll, wenn eine Webseite horizontal aufgebaut ist. Wird die Höhe z.B. mit `height=720` als feste Pixelhöhe definiert, besitzt der Viewport immer die gleiche Höhe, unabhängig vom Ausgabemedium.

### Skalierung

**initial-scale**

Mit der Viewport-Eigenschaft `initial-scale=1.0` wird die Standartskalierung einer Webseite festgelegt. Die Skalierung wird besonders auf mobilen Endgeräten durch doppeltippen genutzt, um entweder einen einzelnen Bereich oder die gesamte Seite im Viewport anzuzeigen. Dadurch können Webseiten, die nicht für kleine Bildschirmgrössen optimiert sind, trotzdem angezeigt werden. Der Wert in diesem Beispiel entspricht einem Zoomlevel von 100% und kann beliebig gewählt werden.

**minimum-scale**

Die Viewport-Eigenschaft `minimum-scale=0.5` legt die kleinst mögliche Skalierung einer Webseite fest. Dies kann hilfreich sein, wenn eine Webseite nicht zu tief skaliert werden soll. Der Wert in diesem Beispiel entspricht einem Zoomlevel von 50% und kann beliebig gewählt werden.

**maximum-scale**

Die Viewport-Eigenschaft `maximum-scale=2.0` legt die grösst mögliche Skalierung einer Webseite fest. Dies kann hilfreich sein, wenn eine Webseite nicht zu hoch skaliert werden soll. Der Wert in diesem Beispiel entspricht einem Zoomlevel von 200% und kann beliebig gewählt werden.

**user-scalable**

Wird die Viewport-Eigenschaft `user-scalable="no"` gesetzt, kann die Skalierung von Nutzerinnen und Nutzern nicht verändert werden. Dies ist unbedingt zu vermeiden, weil der Zugang nicht mehr zu allen Inhalten gewährleistet ist. Don't use it.

[Mehr über den Viewport Meta-Tag lernen...](https://www.w3schools.com/css/css_rwd_viewport.asp)



## Media Queries

### Was ist eine CSS Media Query?

CSS Media Queries gehören zur Grundtechnik des Responsive Designs. Mit Media Queries können verschiedene Merkmale des Ausgabemediums abgefragt werden. So lässt sich z.B. prüfen ob das Ausgabemedium einem bestimten Medientyp entspricht oder die Bildschirmgrösse eine bestimmte Mindestbreite erfüllt. In Deutsch sprechen wir auch von einer Medienabfrage.

### Wie funktioniert eine Media Query?

Eine Medienabfrage kann **Medientypen**, **Medienmerkmale** und **Operatoren** enthalten. Liefert die Abfrage `true` zurück, wird der enthaltene CSS-Code ausgefürt, bei `false` wird der CSS-Code nicht angewendet. Damit können einzelne Elemente oder ganze Bereiche eines Layouts abhängig zum aktuellen Ausgabemedium gestaltet werden.

### Medienabfragen erstellen

Die häufigste Art eine Medienabfrage für eine Webseite zu erstellen, ist die Verwendung des `@media` Tags in einem CSS-Stylesheet. Genau genommen handelt es sich dabei um eine CSS At-Rule, die den Browser anweist den enthaltenen CSS-Code nur dann anzuwenden, wenn eine bestimmte Regel zutrifft. Mit dieser Technik lassen sich ganze Sammlungen von Medienabfragen zu Medientypen und Medienmerkmalen erstellen, um verschiedene Varianten eines Webseiten-Layouts z.B. für grosse und kleine Bildschirme bereit zu stellen.

**Beispiel**

~~~css
/* CSS-Regeln für alle Screens */

@media screen and (min-width: 640px) {
    /* Zusätzliche CSS-Regeln ab einer Breite von 640px des Viewports */
    h1 {
        font-size: 3rem;
    }
}

@media screen and (min-width: 1280px) {
    /* Zusätzliche CSS-Regeln ab einer Breite von 1280px des Viewports */
    h1 {
        font-size: 6rem;
    }
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/YzYoOxp?editors=1100)

### Medientypen selektieren

Eine der wichtigsten Anwendungen von Media Queries ist es, für verschiedene Ausgabemedien unterschiedliche Darstellungen zu ermöglichen. Ein Medientyp ist eine Kategorie, der ein Ausgabemedium zugeortnet ist. Aktuell unterscheidet das W3C zwischen zwei grundlegenden [Medientypen](https://www.w3.org/TR/mediaqueries-4/#media-types).

[Mehr über Medientypen lernen...](https://developer.mozilla.org/en-US/docs/Web/CSS/@media#media_types)

**screen**

`screen` prüft ob es sich um eine Bildschirmausgabe handelt, egal in welcher Grösse. Um ein Design an verschiedene Bildschirmgrössen anzupassen, können in Verbindung mit Operatoren weitere Medienmerkmale abgefragt werden.

**print**

`print` prüft ob es sich um eine Druckvorschau oder Druckausgabe handelt. In einer Druckansicht werden viele Elemente einer Webseite nicht oder in einer anderen Darstellung benötigt. Mit zusätzlichem CSS-Code lassen sich solche entfernen oder für den Druck optimieren. Anwendungsfälle dafür sind Zeitungsartikel oder Rechnugsbelege, die auf Papier einen weiteren Nutzen bieten können.

**all**

`all` gilt für alle Ausgabemedien, ist optional und wird nur im Zusammenhang mit den Operatoren `not` und `only` verwendet.

### Medienmerkmale abfragen

Ein Ausgabemedium unterscheidet sich besonders in seinen Merkmalen. Eines der wichtigsten Merkmale ist die Breite des Viewports. CSS Media Queries erlauben es jedoch weit mehr Merkmale abzufragen. Medienmerkmale sind besondere Eigenschaften. Sie lassen sich in der Regel mit einem Präfix `min-` oder `max-` ergänzen. Eine komplette Liste aller [Medienmerkmale](https://www.w3.org/TR/mediaqueries-4/#mq-features) bietet das W3C.

[Mehr über Medienmerkmale lernen...](https://developer.mozilla.org/en-US/docs/Web/CSS/@media#media_features)

#### Dimension

[**width**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/width)

`width`, `min-width` oder `max-width` prüft die Breite des Viewports.

[**height**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/height)

`height`, `min-height` oder `max-height` prüft die Höhe des Viewports.

[**aspect-ratio**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/aspect-ratio)

`aspect-ratio`, `min-aspect-ratio` oder `max-aspect-ratio` prüft das Verhältnis der Breite zur Höhe.

[**orientation**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/orientation)

`orientation` prüft mit `portrait` oder `landscape` ob die Höhe des Viewports grösser ist als die Breite oder umgekehrt.

#### Darstellungsqualität

[**resolution**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/resolution)

`resolution`, `min-resolution` oder `max-resolution` prüft die Pixeldichte des Ausgabemediums in `dpi`.

[**grid**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/grid)

`grid` prüft mit `1` oder `0` ob ein Ausgabemedium bitmap-basiert arbeitet oder zeichenbasiert.

[**update**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/update-frequency)

`update` prüft mit `none`, `slow` oder `fast` ob und wie schnell die Darstellung eines Ausgabemedium aktualisiert werden kann.

[**overflow-block**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/overflow-block)

`overflow-block` prüft wie das Ausgabemedium mit Inhalten umgeht, die über ein Block-Element hinausragen.

[**overflow-inline**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/overflow-inline)

`overflow-inline` prüft wie das Ausgabemedium mit Inhalten umgeht, die über ein Inline-Element hinausragen.

#### Farbe

[**color**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/color)

`color`, `min-color` oder `max-color` prüft die Farbtiefe des Ausgabemediums in Bits.

[**color-gamut**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/color-gamut)

`color-gamut` prüft mit `srgb`, `p3` oder `rec2020` den unterstützten Farbraum des Ausgabemediums.

[**color-index**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/color-index)

`color-index`, `min-color-index` oder `max-color-index` prüft die Anzahl Einträge in der Farbtabelle (LUT) des Ausgabemediums.

[**monochrome**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/monochrome)

`monochrome`, `min-monochrome` oder `monochrome` prüft die Anzahl Bits pro Pixel im Monochrom-Speicher eines Ausgabemediums.

#### Interaktion

[**any-hover**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/any-hover)

`any-hover` prüft mit `none` oder `hover` ob irgend ein Zeigergerät über einem Element einen hover auslösen kann.

[**any-pointer**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/any-pointer)

`any-pointer` prüft mit `none`, `coarse` oder `fine` ob das Ausgabemedium über irgend ein Zeigergerät verfügt und wie genau dieses arbeitet.

[**pointer**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/pointer)

`pointer` prüft mit `none`, `coarse` oder `fine` ob das Ausgabemedium über ein primäres Zeigergerät verfügt und wie genau dieses arbeitet.

[**hover**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/hover)

`hover` prüft mit `none` oder `hover` ob das primäre Zeigergerät über einem Element einen hover auslösen kann.

#### Accessibility

[**prefers-color-scheme**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-color-scheme)

`prefers-color-scheme` prüft mit `light` oder `dark` das Theme des Ausgabemediums.

[**prefers-reduced-motion**](https://developer.mozilla.org/en-US/docs/Web/CSS/@media/prefers-reduced-motion)

`prefers-reduced-motion` prüft mit `no-preference` oder `reduce` ob für das Ausgabemedium die Reduzierung von Bewegung aktiviert ist.

### Logische Operatoren

Mit den logischen Operatoren `and`, `not`, `only` und Komma `,` können komplexe Medienabfragen erstellt werden. Dazu werden mehrere Regeln in einer Abfrage miteinander verknüpft. Kommagetrennte Regeln, werden einzeln geprüft. Trifft eine davon zu gilt die ganze Abfrage als `true`. Das Komma funktioniert deshalb wie ein `or` Operator.

**If**

Grundsätzlich gilt jede Medienabfrage als If-Abfrage und wird auf `true` oder `false` geprüft.

~~~css
@media (max-width: 600px) {
    /* Der Viewport ist 600px breit oder kleiner */
	html { background: lightgreen; }
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/BaYaeNR?editors=0100)

**And**

Mit einem `and` Operator werden Medientyp und Medienmerkmal miteinander verknüpft. Aber auch mehrere Medienmerkmale können in einer Abfrage miteinander logisch verknüpft werden.

~~~css
@media screen and (min-width: 600px) and (max-width: 900px) {
    /* Der Medientyp ist screen und der Viewport ist mindestens 600px und maximal 900px breit */
	html { background: lightgreen; }
}

@media screen and (600px <= width <= 900px) {
    /* Andere Schreibweise, gleiches Resultat, jedoch neu und von älteren Browsern nicht unterstützt */
	html { background: lightgreen; }
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/RwQwmWR?editors=0100)

**Or**

Kommaseparierte Abfragen werden als `or` Verknüpfung interpretiert.

~~~css
@media (orientation: portrait), (max-width: 600px) {
    /* Der Viewport ist im Portrait Mode oder die Breite ist maximal 600px */
	html { background: lightgreen; }
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/KKQKLVR?editors=0100)

**Not**

Der `not` Operator invertiert die gesamte Medienabfrage und kann nicht auf einzelne Medienmerkmale angewendet werden.

~~~css
@media not all and (max-width: 600px) {
    /* Der Viewport ist breiter als 600px, die Regel ist invertiert */
    html { background-color: lightgreen; }
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/JjpjVqq?editors=0100)

**Only**

Der `only` Operator verhindert, dass ältere Browser keine Medienmerkmale unterstützen und den CSS-Code trotzdem ausführen.

[Mehr über Operatoren lernen...](https://developer.mozilla.org/en-US/docs/Web/CSS/@media#logical_operators)



## Breakpoints

### Was ist ein CSS Breakpoint?

Damit sich ein Webseiten-Layout oder ein ganzes Design optimal an die Bildschirmgrösse anpasst, werden sogenannte Breakpoints verwendet. Breakpoints sind nichts anderes, als aufeinander abgestimmte Media Queries. Sie teilen die Bildschirmgrösse in mehrere Stufen in Verschiedene Design-Varianten auf. In der Praxis wird dies über die Breite des Viewports realisiert.

### Breakpoints festlegen

Ist diese Abstufung nicht bereits durch ein CSS-Framework vorgegeben, gilts es eigene Breakpoints zu definieren. Dazu stellt sich die Frage welche Bildschirmbreiten berücksichtigt werden müssen. Um Sinnvolle Breakpoints zu ermitteln, macht es Sinn die möglichen Endgeräte in Geräteklassen aufzuteilen:

**Geräteklassen**

![Aufteilung in Geräteklassen](Assets/breakpoint-device-classes.svg)

[Beispiel mit CodePen testen...](https://codepen.io/tbz-m293/pen/zYRxqdQ?editors=1100)

### Desktop First

Ein Responsive Design mit Desktop-First-Ansatz wird am einfachsten mit `max-width` aufgebaut. Jeder Breakpoint verringert den Platz auf dem Bildschirm und **das Design wird stufenweise eingeschränkt**.

![Desktop-First-Grafik](Assets/desktop-first-design.png)

Für ein Webdesign bei dem der Umfang bereits zu Beginn feststeht, wird oft der Desktop-First-Ansatz gewählt. Dies ist nicht unbedingt schlecht und bringt in der Zusammenarbeit mit Kunden sogar einige Vorteile: Die Webseite scheint schnell umgesetzt, die Kommuniktation mit der Kundin ist einfach und die grosse Darstellung beeindruckt. Leider stehen den Vorteilen auch einige Nachteile gegenüber: Inhalte die auf grossen Bildschirmen funktionieren, tun dies auf kleinen Bildschirmen oft nicht mehr. Die Lösung heisst abspecken, Inhalte werden gekürzt und Funktionen entfernt. Dies ist mit viel Aufwand verbunden und gschieht meistens nicht im Interesse der Benutzerinnen und Benutzer. Eine Desktop-Version mobiltauglich zu machen, bedeutet also oft den Nutzen zu reduzieren.

**Beispiel mit max-width**

| Breakpoint  | Klasse | Media Query                          |
| ----------- | ------ | ------------------------------------ |
| Extra large | xl     | `@media (min-width: 1500px) { ... }` |
| Large       | lg     | `@media (max-width: 1500px) { ... }` |
| Medium      | md     | `@media (max-width: 1200px) { ... }` |
| Small       | sm     | `@media (max-width: 900px) { ... }`  |
| Extra Small | xs     | `@media (max-width: 600px) { ... }`  |

### Mobile First

Ein Responsive Design mit einem Mobile-First-Ansatz wird am einfachsten mit `min-width` aufgebaut. Jeder Breakpoint schafft mer Platz auf dem Bildschirm, **das Design wird stufenweise erweitert**.

![Mobile-First-Grafik](Assets/mobile-first-design.png)

Mehr als zwei Drittel der Webseiten werden heute auf mobilen Endgeräten konsumiert. Selbst Google bewertet eine Webseite besser, wenn diese für mobile Endgeräte optimiert ist. Ein modernes Webdesign verfolgt deshalb den Mobile-First-Ansatz. Dabei wird ein Design zuerst für die kleinste Darstellung entworfen. Ist mehr Breite vorhanden, wird das Design erweitert und ausgebaut. Dieser Ansatz bringt mehrere Vorteile mit sich: Das Design bleibt übersichtlich, die User Experience kann besser geplant werden und der Designprozess ist in der Regel kostengünstiger, weil er zu einem agilen Vorgehen passt.

**Beispiel mit min-width**

| Breakpoint  | Klasse | Media Query                          |
| ----------- | ------ | ------------------------------------ |
| Extra small | xs     | `@media (max-width: 600px) { ... }`  |
| Small       | sm     | `@media (min-width: 600px) { ... }`  |
| Medium      | md     | `@media (min-width: 900px) { ... }`  |
| Large       | lg     | `@media (min-width: 1200px) { ... }` |
| Extra Large | xl     | `@media (min-width: 1500px) { ... }` |



## Tools

Eine systematische Abstufung durch Breakpoints ist für ein Responsive Design eine grosse Hilfe. Das Design kann für jede Stufe angepasst werden. Um dies zu testen stellen alle Browser entsprechende Werkzeuge zur Verfügung, um den Viewport unabhängig zum Ausgabemedium oder zur Bildschirmgrösse zu testen.

**Browser-Werkzeuge**

![Browser-Werkzeuge](Assets/browser-werkzeuge.png)

**Bildschirmgrössen**

![Browser-Werkzeuge](Assets/viewport-simulator.png)



## Checkpoints

- [ ] Ich kann erläutern wozu Responsive Design dient.
- [ ] Ich kann erklären wie eine CSS Media Query funktioniert.
- [ ] Ich kann geeignete Breakpoints für verschiedene Endgeräte setzen.
- [ ] Ich kann das responsive Verhalten eines Layouts steuern.
- [ ] Ich kann eine responsive Webseite gemäss Vorgaben umsetzen.



## Ressourcen

1. [Responsive Webdesign](https://de.wikipedia.org/wiki/Responsive_Webdesign) (Wikipedia)
2. [Media Queries Level 4](https://www.w3.org/TR/mediaqueries-4/) (W3C Recommendation)
3. [Responsive Design](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout/Responsive_Design) (Mozilla Developer Network)
4. [CSS Responsive](https://www.w3schools.com/css/css_rwd_intro.asp) (W3Schools)



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 12.05.2022, Florian Huber
