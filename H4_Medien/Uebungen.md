# H4 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.



### Übung 1 - Zitat erstellen

Nehmen sie ein Zitat welches sie als wichtig oder lustig empfinden und betten sie es auf einer HTML-Seite ein. Verwenden sie dazu die *figure*- und *figcaption*-Tags.



### Übung 2 - Bildgrössen und Skalierbarkeit

Erstellen sie mit einem Programm (z.B.[gimp](https://www.gimp.org/) oder [draw.io](https://draw.io)) ein Bild mit festen Formen und Text. Sie können zum Beispiel draw.io verwenden und ein abstraktes Bild erstellen oder sie nehmen [dieses Beispiel](image.example.drawio).

1. Exportieren sie das Bild in den drei Formaten
   - Exportieren sie das Bild im *PNG*-Format. Stellen sie sicher, dass sie den Export mit transparentem Hintergrund erstellen.
   - Exportieren sie das Bild im *JPG*-Format.
   - Exportieren sie das Bild im *SVG*-Format. Stellen sie sicher, dass sie den Export mit transparentem Hintergrund erstellen.
2. Schauen sie sich die Dateigrössen an. Was fällt ihnen auf? Halten sie ihre Erkenntnisse fest.
3. Öffnen sie die Dateien in einem Texteditor (z.B. [Notepad++](https://notepad-plus-plus.org/downloads/)). Was fällt ihnen auf? Halten sie ihre Erkenntnisse fest.
4. Erstellen sie eine HTML-Seite mit farbigem Hintergrund und fügen sie alle drei Bilder ein in der Originalgrösse. Zeigen sie, dass nicht alle Dateien einen transparenten Hintergrund haben.
5. Skalieren sie die Bilder auf die doppelte Grösse in ihrem HTML. Was fällt ihnen auf? Halten sie ihre Erkenntnisse fest.



### Übung 3 - Karussell erstellen

Karusselle benötigen meistens eine oder mehrere JavaScript Bibliothek, wobei es selten notwendig ist, dass sie selbst JavaScript programmieren. Sie können die Inhalte einfach aus Anleitungen zusammenfügen. In diesem Beispiel verwenden wir jQuery als Basis und das Slick-Karussell welches mit jQuery im Hintergrund verwendet.

[Hier finden sie die Anleitung](http://kenwheeler.github.io/slick/)

**Teil 1**: Erstellen sie eine HTML-Seite mit einem Karussell. Sie können einfach der Anleitung folgen. Bilder können sie aus den oben referenzierten Webseiten herunterladen oder sie verwenden eigene Bilder. Stellen sie sicher, dass die Bilder frei verfügbar sind.

**Teil 2**: Sie haben bereits gelernt wie sie JavaScript-Dateien mit einem Package Manager verwalten. Ändern sie nun ihre Applikation und verwenden sie einen Package Manager für ihr Karussell. Der HTML-Code sollte sich dabei eigentlich nicht ändern, nur die Art und Weise wie sie die JS-Dateien einbinden.



### Übung 4 - Video einbinden

Verwenden sie aus der Referenz von früher ein Video und fügen sie es auf ihrer Webseite ein. Stellen sie sicher, dass sie das Video verwenden dürfen oder erstellen sie ein eigenes Video.

Konvertieren sie das Video nun in andere unterstützte Formate mit Online-Konvertern (oder eigenen Programmen) und testen sie die verschiedenen Formate. Versuchen sie in ein Format zu konvertieren welches auf ihrem Betriebssystem nicht unterstützt wird. Was geschieht? Halten sie ihre Erkenntnisse fest.

Testen sie nun, wenn sie mehrere **source**-Tags verwenden mit teilweise unterstützten und teilweise nicht unterstützten Formaten. Was geschieht? Halten sie ihre Erkenntnisse fest.

Beispiele für Online Konverter:

- <https://cloudconvert.com/>
- <https://video-converter.com/>



### Übung 5 - Picture Tag verwenden

Wählen sie nun ein Bild im Querformat. Schneiden sie einen Teil davon in der Mitte aus, so dass sie zusätzlich ein Hochformat-Bild erhalten, z.B. so:

![bridge](x_gitressources/Bridge-Example.jpg)

Speichern sie die beiden Bilder auch in verschiedenen Grössen/Dimensionen.

Verwenden sie nun den *picture*-Tag und konfigurieren sie die HTML-Seite so, dass abhängig von der Orientierung und Bildschirmgrösse das richtige Bild angezeigt wird. Verwenden sie [*Lorem Ipsum*-Text](https://www.lipsum.com/) um die Bilder zwischen Text einzubetten. So zeigen sie, dass ihre Wahl sinnvoll ist.