![TBZ-Logo](../x_gitressources/tbz_logo.png)

# HTML-Medien (H4)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann verschiedene Bild-Formate nennen und kann erklären in welchem Kontext ich diese anwenden muss.
- Ich kann den *picture*-Tag anwenden und kann den Unterschied zum *img*-Tag erklären.
- Ich kann den *figure*-Tag anwenden - nicht nur für Medien und kann die semantische Bedeutung nennen.
- Ich kann die verschiedenen Video-Formate nennen, welche von HTML unterstützt werden.
- Ich kann Videos auf einer HTML-Seite einbinden.
- Ich kann die verschiedenen Audio-Formate nennen, welche von HTML unterstützt werden.
- Ich kann Audio auf einer HTML-Seite einbinden.
- Ich kann fremde Medien mit *iframes* auf einer HTML-Seite einbinden.

## Rechtliches

Im Zusammenhang mit Medien müssen rechtliche Aspekte beachtet werden.

### Urheberrecht und Markenrecht

Aus [Wikipedia](https://de.wikipedia.org/wiki/Urheberrecht_(Schweiz)): "Grundsätzlich gilt die (natürliche) Person als Urheber, die das Werk geschaffen hat. Wenn mehrere Personen an der Schaffung eines Werkes mitgeholfen haben, können sie (falls nicht anders vereinbart und soweit sich die einzelnen Beiträge nicht teilen lassen) nur gemeinsam über die Werkverwendung bestimmen. Rechtsverletzungen können von jedem einzelnen Miturheber verfolgt werden". 

In anderen Worten: Wenn sie ohne Erlaubnis Inhalte verwenden, die unter das Urheberrecht fallen, können rechtliche Konsequenzen auftreten. Seien sie also vorsichtig, welche Bilder und Videos sie verwenden.

In der Schweiz gibt es eine ["Impressums-Pflicht"](https://www.weka.ch/themen/marketing-verkauf/online-marketing/e-commerce/article/impressumspflicht-schweiz-so-erstellen-sie-rechtssichere-websites/). Es gibt Inhalte, die müssen zwingend vorhanden sein, wie z.B. Name, Adresse. Die Impressums-Pflicht besteht rechtlich jedoch nur im Bereich E-Commerce, wo Produkte oder Dienstleistungen direkt auf einer Seite angeboten und verkauft werden. Ein Blog oder eine Portfolioseite, die sich lediglich auf die Publikation von Informationen beschränken, zählt beispielsweise [nicht dazu](https://steigerlegal.ch/2012/04/01/impressumspflicht-im-e-commerce-fragen-und-antworten/). Trotzdem empfielt sich für jede Webseite ein Impresssum zu erstellen, damit eine einfache Kontaktaufnahme möglich ist.

### Lizenzen

Viele Werke (spezielle digitale Medien) werden unter "freien" Lizenzen zur Verfügung gestellt, so dass sie die Bilder und Videos verwenden dürfen. Viele bekannte Lizenzen decken Open Source Software ab. Andere aber auch Bilder und Medien, z.B. die [Creative Commons License](https://creativecommons.org/). Sie wählen [welche Attribute](https://creativecommons.org/choose/) die Lizenz beinhalten soll. So ist es z. B. möglich, dass ein Objekt für private Zwecke verwendet werden darf, aber nicht für kommerzielle. 

Lesen sie auf den freien Plattformen immer kurz nach wie sie die Bilder verwenden dürfen, z.B:

- [Unsplash](https://unsplash.com/license)
- [Pexels](https://www.pexels.com/license/)

Empfohlen wird jeweils sowieso, dass sie die Referenzen im Impressum hinzufügen.

## Bilder und Bildformate

In der folgenden Tabelle finden sie die gängigsten Bild-Formate wie sie in HTML verwendet werden. Es gibt Unterschiede im Verwendungszweck.

| Format | Raster/Vektor | Transparenz | Anwendungszweck |
| --------- | --------- | ---- | --------------------------------------------------------------------------------------------------------------------- |
| [JPG/JPEG](https://de.wikipedia.org/wiki/JPEG) | Raster | Nein | Photographie, Detaillierte Bilder mit Übergängen und Farbverläufen. JPG-Bilder haben einen Verlust durch die Komprimierung, der aber bis zu einer bestimmten Komprimierungsgrösse visuell nicht sichtbar ist. |
| [PNG](https://de.wikipedia.org/wiki/Portable_Network_Graphics) | Raster | Ja | Bilder mit scharfen Kanten, Abstufungen und Logos. PNG-Bilder werden verlustfrei komprimiert, aber sind entsprechend grösser als JPG. Schrift und Logos bleiben dafür klar von anderen Elementen getrennt. |
| [GIF](https://de.wikipedia.org/wiki/Graphics_Interchange_Format) | Raster | Ja | Animationen. Das GIF-Format kann auch als PNG-Ersatz dienen, wobei PNG die neuere Techonologie ist und besser (verlustfrei) komprimiert. Darum werden GIFs nur noch für bewegte Bilder verwendet. Dabei ist die Farbtiefe jeweils tief, weil sich sonst die Bilder nicht mehr gut komprimieren lassen. |
| [SVG](https://de.wikipedia.org/wiki/Scalable_Vector_Graphics) | Vektor | Ja | Logos, Schriften, etc. Bei SVG handelt es sich um - via XML beschriebene - Vektorgrafik und ist somit verlustfrei skalierbar. Ausserdem können die XML-Elemente mit CSS manipuliert werden. |



### *figure*-Tag

Sie kennen bereits den *img*-Tag. Um einer Webseite zusätzliche Semantik zu geben, wurde mit HTML 5 die Tags *figure* und *figcaption* eingeführt. Diese haben nicht das Ziel einen *img*-Tag zu ersetzen, sondern Informationen über den Inhalt zu liefern. Normalerweise wird der *figure*-Tag zusammen mit Bildern oder Illustrationen eingesetzt und enthalten eigenständigen Inhalt, welcher zwar in Beziehung zur Seite steht, aber dessen Position unabhängig vom restlichen ist. Der *aside*-Tag erfüllt eine ähnliche Funktion, enthält aber Inhalt, welcher zwar in Beziehung zur Seite steht, aber nicht relevant ist.

[Beispiel mit formatierten Captions auf Codepen](https://codepen.io/nuy/pen/mdpPzOw)

![caption](./x_gitressources/figurecaption.png)

Quellen:

- <https://www.w3schools.com/tags/tag_figure.asp>
- <https://www.w3schools.com/tags/tag_figcaption.asp>

### *picture*- vs *img*-Tag

Neben dem *img*-Tag gibt es auch den *picture*-Tag, welcher in allen modernen Browsern unterstützt wird. Damit lassen sich verschiedene Umgebungen einfach und semantisch abbilden, z.B. können sie unterschiedliche Bilder wählen für die Portrait- oder Landscape-Orientierung und für verschiedene Bildschirm-Auflösungen. [Lesen sie Details in diesem Artikeln nach.](https://blog.bitsrc.io/why-you-should-use-picture-tag-instead-of-img-tag-b9841e86bf8b).

Vieles können sie auch mit CSS lösen. Der *picture*-Tag ist aber keine Konkurrenz zu CSS, sondern bietet die Möglichkeit ihrer Webseite vertiefte Semantik zu vermitteln.

~~~~html
<picture id="headerImage">
    <source media="(min-width:1200px)" srcset="image_wide.jpg" />
    <source media="(min-width:800px)" srcset="image_medium.jpg" />
    <source media="(min-width:786px)" srcset="image_small.jpg" />
    <img src="image_medium.jpg" alt="Example Image" />
</picture>
~~~~

Quellen:

- <https://www.w3schools.com/tags/tag_picture.asp>
- <https://blog.bitsrc.io/why-you-should-use-picture-tag-instead-of-img-tag-b9841e86bf8b>
- Free images: <https://pixabay.com/photos/>
- Free images: <https://unsplash.com/>

### Anwendungen

Gute Anwendungen von Bildern finden sie in Karussellen, Header Sliders und Galerien. In den meisten Fällen benötigen sie Unterstützung durch JavaScript Frameworks, wie zum Beispiel jQuery, Bootstrap, etc. Sie können diese Werkzeuge aber einfach anwenden, ohne dass sie JavaScript lernen müssen - einfach indem sie die Schritte der Anleitungen ausführen. Später bei den Übungen können sie ein Karussell erstellen.



## Video und Videoformate

Im HTML Standard sind die drei Video-Formate *MP4*, *WebM* und *Ogg* definiert, wobei nicht alle in jedem Browser unterstützt werden. Verwenden sie das Format *MP4* mit dem *H264* Encoding für die beste Verbreitung. Gute Video-Unterstützung kam erst mit HTML 5. Vorher wurden die bewegten Bilder oft mit einem Adobe Flash Format eingebunden, wobei der Browser dazu ein Plugin benötigte.

Um alle Browser optimal mit dem Video-Format zu unterstützen, können sie Videos in mehreren Formaten einbinden und der Browser entscheidet, welches er verwenden wird indem er sich von oben nach unten durch die Liste arbeitet.

~~~~html
<video controls autoplay name="media">
    <source src="video.mp4" type="video/mp4">
    <source src="video.webm" type="video/webm">
    <source src="video.ogg" type="video/ogg">
    <!-- weitere formate möglich --->
</video>
~~~~

Sie können [hier ein Beispiel in Codepen testen](https://codepen.io/nuy/pen/xxpVzQE).

![VideoWithControls](x_gitressources/videoWithControls.png)

Quellen:

- Video by [Danilo Riba](https://pixabay.com/users/daniloriba-18260006/) from [Pixabay](https://pixabay.com/)
- <https://en.wikipedia.org/wiki/HTML5_video>
- <https://www.w3schools.com/html/html_media.asp>
- <https://www.w3schools.com/tags/tag_video.asp>
- <https://www.w3schools.com/tags/tag_source.asp>
- <https://www.w3schools.com/html/html5_video.asp>
- Free videos: <https://pixabay.com/videos/>



## Audio und Audioformate

Audio-Einbettungen folgen der gleichen Logik wie Video. Die offiziell unterstützten Formate sind *MP3*, *WAV* und *OGG*. Verwenden sie *MP3* für eine optimale Browser-Unterstützung.

Quellen:

- <https://www.w3schools.com/tags/tag_audio.asp>
- Free sounds: <https://freesound.org/browse/>



## Frames

In HTML 4 gab es noch das Konzept von Frames und Framesets. Ein Frameset definierte mehrere Frames, wobei jedes Frame eine eigenen HTML-Seite enthielt, die lose gekoppelt waren. So wurden eine seitliche Navigation oder die Kopfzeile separiert und wenn man auf einen Link klickte, wurde der Hauptframe neu geladen mit dem neuen Inhalt. **Frames und Framesets sind seit HTML 5 nicht mehr unterstützt**.

Geblieben ist aber der *iframe*-Tag (inline Frame), welcher es erlaubt externe Inhalte in der eigenen HTML-Seite anzuzeigen. Sie öffnen damit also eine Seite in ihrer Seite. Angewandt werden iframes meistens nur noch für das einbetten von externen Medien, ohne dass sie die Quelldateien haben, z.B. youtube oder vimeo. Nach wie vor können sie so aber HTML-Seiten einbinden, die dann in sich geschlossen ausgeführt werden.

~~~~html
<html>
    <head></head>
    <body>
        <p>Dies ist mein Inhalt, aber folgend binde ich ein youtube-Video ein</p>
        <p><iframe width="427" height="240" src="https://www.youtube.com/embed/dQw4w9WgXcQ?autoplay=1" title="YouTube video player" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></p>
        <p><strong>Falls sie nicht wissen wieso das Video oben gespielt wird, lesen sie <a href="https://en.wikipedia.org/wiki/Rickrolling">hier nach</strong></a></p>
    </body>
</html>
~~~~

[Hier finden sie das genannte Beispiel auf Codepen](https://codepen.io/nuy/pen/qBpZyLy)

Quellen:

- <https://www.w3schools.com/tags/tag_iframe.asp>
- <https://developers.google.com/youtube/player_parameters>



## Canvas

Innerhalb eines Canvas können sie 2D und 3D Objekte zeichnen. Sie verwenden dabei JavaScript und eine entsprechende Bibliothek. Die Links dazu finden sie bei den Quellen.

Wir werden hier aber nicht weiter auf Canvas eingehen. Sie können aber [in diesem CodePen Beispiel selbstständig üben](https://codepen.io/nuy/pen/oNpxPXm).

![Canvas](x_gitressources/canvas.png)

Quellen:

- <https://www.w3schools.com/tags/tag_canvas.asp>
- <https://www.w3schools.com/tags/ref_canvas.asp>



## Checkpoints

- [ ] Ich kann verschiedene Bild-Formate nennen und kann erklären in welchem Kontext ich diese anwenden muss.
- [ ] Ich kann den *picture*-Tag anwenden und kann den Unterschied zum *img*-Tag erklären.
- [ ] Ich kann den *figure*-Tag anwenden - nicht nur für Medien und kann die semantische Bedeutung nennen.
- [ ] Ich kann die verschiedenen Video-Formate nennen, welche von HTML unterstützt werden.
- [ ] Ich kann Videos auf einer HTML-Seite einbinden.
- [ ] Ich kann die verschiedenen Audio-Formate nennen, welche von HTML unterstützt werden.
- [ ] Ich kann Audio auf einer HTML-Seite einbinden.
- [ ] Ich kann fremde Medien mit *iframes* auf einer HTML-Seite einbinden.



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 15.05.2022, Yves Nussle
