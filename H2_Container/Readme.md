![TBZ-Logo](../x_gitressources/tbz_logo.png)

# HTML-Container (H2)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann den Unterschied zwischen Wireframe, Mockup und Prototyp erklären.
- Ich kann alle HTML-Elemente nennen, die als Container dienen.
- Ich kann ein Wireframe für eine Webseite erstellen.
- Ich kann eine Seitenstruktur sematisch korrekt aufbauen.
- Ich kann eine Inhaltsstruktur semantisch korrekt aufbauen.
- Ich kann die Container-Tags ohne semantische Bedeutung nennen.



## Begriffe

### Was ist ein Container?

Bisher haben sie sich mit den HTML-Tags befasst, die zur Darstellung der Inhalte dienen (Paragraphen, Bilder, etc). In diesem Teil geht es darum, dass sie sich mit der Struktur einer Seite befassen und Inhalte in definierbare und logische Bereiche einteilen können.

Wie sie Container korrekt anwenden und platzieren, lernen sie dann zusammen mit den entsprechenden CSS-Anweisungen im nächsten Kapitel.

### Wireframes, Mockups und Prototypes

Sie werden in diesem Modul immer wieder auf diese Begriffe stossen. Folgend sehen sie kurze Begriffserklärungen, die aber nicht weiterführen. Im Modul 322 werden sie denen wieder begegnen und dann werden sie auch ausführlich besprochen.

Die folgenden Zitate ([Quelle](https://www.visual-paradigm.com/guide/ux-design/wireframe-vs-storyboard-vs-wireflow-vs-mockup-vs-prototyping/)) zeigen klar was die Unterschiede der drei Begriffe sind.

- "While a wireframe mostly represents a screen or webpage structure, a mockup shows how the actual screen is going to look like".
- "A prototype is a draft version of a product that allows you to explore your ideas and show the intention [...] before investing time and money into development".

Stellen sie sich die drei Begriffe also auch wie folgt vor:

- Wireframe: Abstrakte Idee
- Mockup: Design der Ideee
- Prototype: Interaktive Version dess Mockups. Die Übergänge sind oft fliessend, so spricht man z.B. manchmal auch von klickbaren Mockups anstelle von Prototypen.

Folgend zwei Beispiele zu Wireframe und Mockup. Ein Beispiel zu einem Prototyp wäre dann, wenn man das Mockup interaktiv gestaltet. Oft ist es so, dass das Mockup nicht dem Wireframe entspricht, weil sich die Idee weiterentwickelt bis zur Umsetzung des Mockups.

![wireframeMockup](x_gitressources/wireframe-mockup.png)



## Container für die Seitenstruktur

### Header, Footer und Main

Fast jede HTML-Seite hat einen **Header** und **Footer**, die unterschiedlich stark ausgeprägt sind. Stellen sie sich folgendes Wireframe vor, bei dem die Farben in drei Bereiche teilen:

- grün: Kopfzeile (Header)
- gelb: Inhalt (Main)
- Violett: Fusszeile (Footer)

![Wireframe](x_gitressources/wireframe-example1.png)

Diese drei Bereiche können mit den drei Tags ***header, main, footer*** abgegrenzt werden. **Achtung**: Verwechseln sie den Tag **header** nicht mit dem Meta-Tag **head**

**Beispiel**

~~~html
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <header>
            Hier kommt ihre Kopfzeile mit Logo, Navigation, etc
        </header>
        <main>
            Hier erscheint ihr kompletter Inhalt
        </main>
        <footer>
            &copy; Imaginary Company
        </footer>
    </body>
</html>
~~~

[Beispiel mit CodePen testen](https://codepen.io/nuy/pen/bGoXRma)



## Container für die Inhaltsstruktur

Es gibt verschiedene Tags, die dem Inhalt **semantische Bedeutung** geben. Dies bedeutet, dass man aufgrund des verwendeten Tags auf den Inhalt schliessen kann.

### Article

`article` Der Inhalt eines *article*-Tags ist ein **Artikel** und besteht normalerweise aus Titel, Text und evtl. weiterführenden Links.

### Aside

`aside` Der *aside*-Tag kennzeichnet **zusätzlichen Inhalt**, der weiterführend sein kann, z.B. Aktionen bei Webshops, oder Link-Listen für das Hauptthema. Im Beispiel oben sind die Blöcke auf der rechten Seite *aside*-Bereiche.

### Nav

`nav` Der *nav*-Tag wird für **Navigationen** verwendet. Oft finden sie die Top-Navigation und die Haupt-Navigation auf einer Seite.

### Section

`section` Der *section*-Tag ist ein **generischer Tag** und wird verwendet, falls kein anderer Tag spezifischere semantische Bedeutung gibt. Anwendungsbereiche wären zum Beispiel Suchresultate oder Kartographieinhalte oder auch die Strukturierung von Inhalten

![TagUsageNewspaper](x_gitressources/main-section-article.png)

Das Bild zeigt den Quellcode einer schweizer Tagszeitung und die Anwendung der Tags. Hier wird der *section*-Tag verwendet, um alle Artikel zusammenzufassen.

Hier ein eigenes vereinfachtes Beispiel. Beachten sie, dass wir keine Layout-Informationen mitgeben zu diesem Zeitpunkt, sondern nur die Inhalte strukturieren. Aus diesem Grund erscheinen alle Inhalte untereinander.

**Beispiel**

~~~html
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <header>
            <nav id="TopNavigation">Warenkorb&nbsp;&nbsp;&nbsp;<a href="#">Hans Muster</a></nav>
            Hier kommt ihre Kopfzeile mit Logo, Navigation, etc
            <br />
            <nav id="MainNavigation">Home | Kategorien | Ausverkauf | Aktionen</nav>
        </header>
        <main>
            <h1>Kategorie A</h1>
            <nav id="LeftNavigation">
                Section 1<br />
                Section2 <br />
                ...
            </nav>
            <section>
                <article>
                    <img src="https://ch-tbz-it.gitlab.io/Stud/m293assets/H2_Container/dummy.png" />
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
                <article>
                    <img src="https://ch-tbz-it.gitlab.io/Stud/m293assets/H2_Container/dummy.png" />
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
                <article>
                    <img src="https://ch-tbz-it.gitlab.io/Stud/m293assets/H2_Container/dummy.png" />
                    <br />
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
            </section>
            <section>
                <aside>
                    <h4>Tagesaktion</h4><br />
                    <img src="https://ch-tbz-it.gitlab.io/Stud/m293assets/H2_Container/dummy.png" /><br />
                    Kurzbeschreibung
                </aside>
                <aside>
                    <h4>Oft gekauft</h4><br />
                    <img src="https://ch-tbz-it.gitlab.io/Stud/m293assets/H2_Container/dummy.png" /><br />
                    Kurzbeschreibung
                </aside>
            </section>
        </main>
        <footer>
            <section>
                <h5>Hilfe und Kontakt</h5><br />
                <a href="#"></a>Kontakt</a><br />
                <a href="#">Support Center</a>
            </section>
            <section>
                <h5>Service</h5><br />
                <a href="#"></a>Gutscheine</a><br />
                <a href="#">Lieferung</a>
            </section>
        </footer>
    </body>
</html>
~~~

[Beispiel mit CodePen testen](https://codepen.io/nuy/pen/xxXvreb)



## Neutrale Container

### Div

Der *div*-Tag ist der häufigst benutzte Tag bei Webseiten. Leider wird er auch oft falsch angewandt, z.B. zum strukturieren von Inhalten für die aber ein spezifischer Tag existiert (section, aside, article, etc). Ein *div*-Tag hat keine spezifische Bedeutung und repräsentiert die enthaltenen Tags. Er findet Verwendung um Elemente als Block darzustellen und zu positionieren. Bei späteren Kapiteln zur Darstellung von Inhalten, wird er weiter ausgeführt und Beispiele gezeigt wie der Tag effizient und sachgerecht angewandt wird.

### Span

Während der *div*-Tag ein Block definiert mit Inhalten, wird der *span*-Tag *inline* verwendet, also innerhalb eines Texts um einen Teil davon zu repräsentieren, z.B. mit unterschiedlichem Schrifttyp oder Farbe.

[Mehr über Block- und Inline-Container lernen...](https://www.w3schools.com/html/html_blocks.asp)

**Beispiel**

~~~html
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <main>
            <h1>Demo span-Tag</h1>
                <article>
                    <h2>Artikel 2</h2>
                    Lorem ipsum dolor sit amet, <span class="font">consectetur</span> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
                <article>
                    <h2>Artikel 2</h2>
                    Lorem ipsum dolor sit amet, consectetur <span class="dark">adipisicing</span> elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
                <article>
                    <h2>Artikel 3</h2>
                    Lorem ipsum dolor sit amet, <span class="light">consectetur</span> adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
                </article>
        </main>

    </body>
</html>

~~~

[Beispiel mit CodePen testen](https://codepen.io/nuy/pen/PoJMKXx)



## Checkpoints

- [ ] Ich kann den Unterschied zwischen Wireframe, Mockup und Prototyp erklären.
- [ ] Ich kann alle HTML-Elemente nennen, die als Container dienen.
- [ ] Ich kann ein Wireframe für eine Webseite erstellen.
- [ ] Ich kann eine Seitenstruktur sematisch korrekt aufbauen.
- [ ] Ich kann eine Inhaltsstruktur semantisch korrekt aufbauen.
- [ ] Ich kann die Container-Tags ohne semantische Bedeutung nennen.



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 15.05.2022, Yves Nussle
