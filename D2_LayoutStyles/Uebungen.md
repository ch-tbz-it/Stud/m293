# D2 Übungen

> Machen Sie die Übungen ohne Boilerplate, Framework oder ähnliches. Schreiben Sie 100% eigenen Code und präsentieren Sie das Ergebnis Ihrer Lehrperson. Geben Sie das Ergebnis als ZIP-Datei ab.



### Übung 1 - Cover Grid

Erstellen Sie eine einfache Webseite für Filme, Musikalben oder Produkte und nutzen Sie zur Darstellung ein Grid-Layout. Richten Sie die Höhe der Grid-Items an den Zeilen aus und definieren Sie Abstände zwischen Spalten und Zeilen. Im Ordner **Examples** finden Sie [eins](Examples/Movie-Grid-Beispiel.pdf), [zwei](Examples/Product-Grid-Beispiel.pdf), [drei](Examples/Music-Grid-Beispiel.pdf) Beispiele als Screenshot.

Erklären Sie wie Ihr Grid aufgebaut ist und wie es funktioniert.



### Übung 2 - Copy Cat

Erstellen Sie eine optische Kopie einer beliebigen **Marken-Webseite** (z.B. Apple, Microsoft, Tesla, Nike, usw.). Es ist mit HTML und CSS alles erlaubt. Inhalte wie Text und Bilder können Sie 1:1 kopieren. Versuchen Sie es **möglichst echt** aussehen zu lassen.

Erklären Sie nach welchen Design-Regeln die Seite aufgebaut ist.