![TBZ-Logo](../x_gitressources/tbz_logo.png)

# CSS-Layouts (D2)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

1. Ich kann erläutern wozu ein CSS-Layout dient.
2. Ich kann erklären wie ein Layout aufgebaut ist.
3. Ich kann die Style-Eigenschaften für Flow, Positionierung und Floating nennen.
4. Ich kann das Verhalten einer Flexbox steuern.
5. Ich kann ein Layout mit Grid-System umsetzen.



## Einführung

In [CSS-Grundlagen](../D1_BasicStyles/Readme.md) haben wir uns die Formatierung von Text und Boxen angeschaut. In diesem Teil geht es um die Anordnung und Positionierung von Boxen. Wir schauen uns CSS-Techniken wie Flexbox und Grid an und wie damit moderne Layouts erstellt werden können.

### Was sind Layouts?

Eine Webseite besteht oft aus mehreren Inhalten. Deshalb werden im Webdesign sogenannte Layouts verwendet. Als [Layout](https://de.wikipedia.org/wiki/Layout) bezeichet man die Anordnung von verschiedenen Inhalten auf einer Seite. Dazu wird eine Seite in mehrere Bereiche aufgeteilt und die Grösse und Position dieser Bereiche definiert. Auch dafür bietet CSS eine Reihe von Eigenschaften, die wir im folgenden näher betrachten.

### Wie funktioniert ein CSS-Layout?

Um eine Webseite in Bereiche aufzuteilen, wird eine Struktur aus [HTML-Container](../H2_Container/Readme.md) erstellt. Sie dient als Grundgerüst einer Webseite. Container wie `header`, `main` und `footer` werden oft als Grundstruktur einer Seite verwendet. Sie teilen die Seite grob auf und können durch weitere Container in noch kleinere Bereiche aufgeteilt werden oder mit Inhalt befüllt werden. Ein Layout ist also eine Verschachtelung von HTML-Containern.

> **Hinweis:** Als es in CSS noch keine Möglichkeiten gab Layouts zu beschreiben, wurden oft HTML-Tabellen dafür benutzt. Verwenden Sie Tabellen ausschliesslich für Tabellen und niemals für Layouts.

Mit CSS wird das Gestaltungsmuster beschrieben, wie die Container dargestellt werden. Eines der häufigsten Gestaltungsmuster ist das Spalten-Layout, welches die Container mit der Eigenschaft `float` in einer, zwei oder drei Spalten nebeneinander anordnen lässt.

[Mehr über CSS-Layouts lernen...](https://developer.mozilla.org/en-US/docs/Learn/CSS/CSS_layout)

**1-Spalten-Layout**

~~~html
<header>Header</header>
<main>
    <article>Artikel</article>
</main>
<footer>Footer</footer>
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/PoORoeN?editors=1100)

**2-Spalten-Layout**

~~~html
<header>Header</header>
<main>
    <article>Artikel</article>
    <aside>Kategorien</aside>
</main>
<footer>Footer</footer>
~~~

~~~css
main {
    display: flow-root; /* Do not use Clearfix */
}

article, aside {
    width: 50%; /* Spaltenbreite */
    float: left;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/ZEaxEEy?editors=1100)

**3-Spalten-Layout**

~~~html
<header>Header</header>
<main>
    <aside>Navigation</aside>
    <article>Artikel</article>
    <aside>Kategorien</aside>
</main>
<footer>Footer</footer>
~~~

~~~css
main {
    display: flow-root; /* Do not use Clearfix */
}

article, aside {
    width: 33.33%; /* Spaltenbreite fix */
    width: calc(100% / 3); /* oder berechnet */
    float: left;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/eYeVqQO?editors=1100)



## Flow Layout

### Display

HTML-Elemente werden immer nacheinander angeordnet. Die CSS-Eigenschaft `display` unterscheidet mit `inline` und `block` zwei grundsätzliche Richtungen. Inline-Elemente werden versucht **horizontal** in Flussrichtung **nebeneinander** darzustellen, Block-Elemente **vertikal** in Flussrichtung **übereinander**.

[Mehr über CSS-Display lernen...](https://www.w3schools.com/css/css_display_visibility.asp)

### Float

Mit der CSS-Eigenschaft `float` können Elemente aus diesem Fluss entfernt und mit `left` entweder links oder mit `right` entsprechend rechts platziert werden. Das Element bleibt dennoch Teil des Flusses, indem alle Inline-Elemente das Floating-Element umfliessen. Floating ist eine der meist verbreiteten Layout-Techniken, da mehrere Block-Elemente **horizontal** nebeneinander dargetsellt werden können, sofern ihre Gesamtbreite nicht grösser als die umschliessende Container-Breite ist.

[Mehr über CSS-Float lernen...](https://www.w3schools.com/css/css_float.asp)

### Overflow

Wenn Elemente nicht mehr Teil des Flusses sind kann es vorkommen, dass ein Element in der Darstellung über ein anderes Element hinausragt. Dank der CSS-Eigenschaft `overflow` können wir dieses Anzeigeverhalten steuern. Die Werte `visible`, `hidden`, `scroll` und `auto` legen fest, ob ein überstehender Inhalt sichtbar ist, abgeschnitten wird oder gescrollt werden kann.

[Mehr über CSS-Overflow lernen...](https://www.w3schools.com/css/css_overflow.asp)



## Positional Layout

### Wie funktioniert CSS-Position?

Eine weitere Möglichkeit ist die Positionierung von Elementen. Die CSS-Eigenschaft `position` erlaubt es HTML-Elemente ganz **aus dem Layout-Fluss zu nehmen**. Es handelt sich um eine Technik in der bestimmt werden kann, wo ein Element platziert werden soll und über welchen anderen Elementen es angezeigt werden soll.

[Mehr über CSS-Position lernen...](https://www.w3schools.com/css/css_positioning.asp)

### Static

`static` beschreibt das **Standard-Verhalten** eines HTML-Elements. Das Element wird im normalen Fluss positioniert und verhält sich wie ein Inline- oder Block-Element.

~~~css
p {
    position: static; /* default*/
}
~~~

### Relative

`relative` bedeutet, dass ein Element **relativ zu seiner normalen Position** im Layout-Fluss angezeigt wird. Um ein Element von diesem Null-Punkt aus zu verschieben, stellt CSS die Eigenschaften `top`, `bottom`, `left` und `right` zur Verfügung. Wird eine oder mehrere dieser Eigenschaften gesetzt, wird das Element um diesen Wert nach oben, unten, links oder rechts verschoben. Dies hat keinen Einfluss auf andere Elemente, da an der ursprünglichen Position Platz gelassen wird.

~~~css
p {
    position: relative;
    top: 2em;
    left: 2em;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/dyZmRXw?editors=1100)

### Absolute

`absolute` wird dann verwednet, wenn ein Element an einem **bestimmten Punkt positioniert** werden soll. Das Element wird aus dem normalen Fluss gelöst und unabhängig verschoben. Dabei können andere Elemente verdeckt werden. Diese verhalten sich so, als ob das Element nicht vorhanden wäre und lassen keinen Platz mehr für das Element. Die Eigenschaften `top`, `bottom`, `left` und `right` legen den Abstand zum entsprechenden Rand fest.

~~~css
p {
    position: absolute;
    top: 3em;
    left: 3em;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/abVYqrV?editors=1100)

### Fixed

`fixed` erlaubt es ein Element **an einem festen Punkt zu fixieren**. Die Verschiebung orientiert sich am Viewport. Das Element wird aus dem normalen Fluss gelöst und bleibt auch beim Scrollen an seiner fest definierten Position. Beim Drucken wird das Element auf jeder Seite an der positionierten Stelle angezeigt. Dies ist besonders für Menus und Navigations-Elemente hilfreich, die als Leiste am oberen Bildrand angebracht sind.

~~~css
p {
    position: fixed;
    top: 0;
    left: 0;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/abVYYmG?editors=1100)

### Sticky

`sticky` ist ein neuerer Wert und **eine Mischung** aus `relative` und `fixed`. Das Element wird vom normalen Fluss aus verschoben und hat keinen Einfluss auf andere Elemente, solange es sich innerhalb des Viewports befindet. Erreicht das Element beim scrollen einen bestimmten Punkt, wird das es aus dem Fluss entfernt und an seiner definierten Position angeheftet. Dieser Punkt wird mit den Eigenschaften `top`, `bottom`, `left` und `right` deklariert.

~~~css
p {
    position: sticky;
    top: 0;
    left: 0;
}
~~~
[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/QWOmmOd?editors=1100)



## Flexbox Layout

### Was ist eine CSS-Flexbox?

Eine Flexbox ist ein eindimensionales Layout-System zur Anordnung von Elementen in Zeilen **oder** Spalten. Die Layout-Elemente sind **in ihrer Ausrichtung und Grösse flexibel**. Sie werden gestreckt um zusätzlichen Platz zu füllen und schrumpfen, um in kleinere Räume zu passen. Dies können sie immer nur in eine Richtung, also entweder **horizontal** oder **vertikal**.

Lange Zeit waren `float` und `position` die einzigen zuverlässigen Möglichkeiten CSS-Layouts zu erstellen. Sie funktionieren bis heute wunderbar, nur bieten sie auch gewisse Einschränkungen bei der Umsetzung von Layouts.

[Mehr über CSS-Flexbox lernen...](https://www.w3schools.com/css/css3_flexbox.asp)

### Flex-Container

Ein Flexbox-Layout besteht aus einem Layout-Container und mehreren Layout-Items. Als Layout Container kann jedes Block-Element verwendet werden, das mit `display: flex;` zu einem Flexbox-Container deklariert wird. Um eine semantische Struktur zu schaffen, sollte ein Container-Element bevorzugt werden.

[Mehr über Flexbox-Container lernen...](https://www.w3schools.com/css/css3_flexbox_container.asp)

**Zeilen oder Spalten**

Die Eigenschaft `flex-direction` legt die Achse fest an der das Layout ausgerichtet wird. Der Wert `row` bedeutet, dass die Achse **horizontal** in Zeilen verläuft. Flex-Elemente werden somit nebeneinander angeordnet und teilen sich die zur Verfügung stehende Breite. Der Wert `column` definiert die Achse als **vertikal** in Spalten. Flex-Elemente werden somit untereinander ausgerichtet. `flex-direction` bietet mit `row-reverse` und `column-reverse` zudem die Möglichkeit Flex-Elemente **in umgekehrter Reihenfolge** darzustellen.

**Wrapping**

Die Eigenschaft `flex-wrap` legt fest, ob die Elemente in einem Flexbox-Container auf einer Linie liegen, oder bei bedarf auf mehrere Zeilen verteilt werden können. Mit dem Wert `wrap` wird dieser Zeilenumbruch erlaubt, mit `nowrap` wird kein Umbruch erlaubt, was dem Standardwert entspricht.

**Kurzform**

Die Eigenschaft `flex-flow` ist eine Kurzform für `flex-direction` und `flex-wrap`, **beide in einer Deklaration**. Der Standardwert für ein Element ist `flex-flow: row nowrap`.

**Beispiel**

~~~css
section {
    display: flex;
    flex-direction: row; /* default */
    flex-wrap: nowrap; /* default */
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/yLPKwrp?editors=1100)

### Flexbox-Items

In einem Flexbox-Container gelten alle **direkt** folgenden Elemente als Flexbox-Items. Sie werden an der vom Container definierten Achse ausgerichtet und teilen den vorhandenen Raum automatisch unter sich auf.

[Mehr über Flexbox-Items lernen...](https://www.w3schools.com/css/css3_flexbox_items.asp)

**Vergrösserung**

`flex-grow` beschreibt in welchem Verhältnis sich ein Flexbox-Item gegenüber den anderen Flexbox-Items vergrössern soll. Der Wert kann eine beliebige Zahl sein. Als Standardwert wird `0` verwendet.

**Verkleinerung**

`flex-shrink` beschreibt in welchem Verhältnis sich ein Flexbox-Item gegenüber den anderen Flexbox-Items vergrössern soll. Der Wert kann eine beliebige Zahl sein. Als Standardwert wird `1` verwendet.

**Mindestlänge**

`flex-basis` beschreibt die Breite oder Höhe eines Flexbox-Items, die es entlang der festgelegten Achse mindestens haben soll. Der Wert kann eine beliebige Länge in `%`, `px`, `em` oder anderer Einheit sein. Als Standardwert für diese Eigenschaft wird `auto` verwendet.

**Kurzform**

Die Eigenschaft `flex` ist eine Kurzform für `flex-grow`, `flex-shrink` und `flex-basis` innerhalb einer Deklaration. Der Standardwert für ein Element ist `flex: 0 1 auto`.

**Beispiel**

~~~css
section {
    display: flex;
    flex-direction: row; /* default */
    padding: 1em;
}

article {
    background-color: lemonchiffon;
    flex: 1;
    margin: 1em;
    padding: 2em;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/jOaxMZx?editors=1100)

Eine sehr gute "grafische" Erklärung / Zusammenfassung zu dem Flex-Thema ist bei [css-tricks.com](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) zu finden.

## Grid Layout

### Was ist ein CSS-Grid?

Ein Grid ist ein **zweidimensionales** Layout-System. Es ermöglicht die Anordnung und Ausrichtung von Inhalten innerhalb eines Rasters, bestehend aus **Spalten und Zeilen**. Das CSS-Grid bietet umfangreiche und gezielte Eigenschaften, um komplexe Layouts darzustellen. Es handelt sich um eine relativ neue Layout-Technik, doch CSS-Grids werden bereits von allen modernen Browsern unterstützt.

### Wie funktioniert ein Grid-Layout?

Ein Grid ist ein Raster aus horizontalen und vertikalen Linien, in dem wir unsere Design-Elemente ausrichten. Man könnte auch sagen die **Design-Elemente rasten ein**, denn sämtliche Container-Elemente liegen genau auf den Linien des Grids. Dadurch springen Elemente nicht herum oder verändern die Breite, wenn wir durch eine Webseite navigieren. **Das Design wird konsistent**.

![Grid-System](Assets/grid-layout-mdn.png)

[Mehr über CSS-Grid lernen...](https://www.w3schools.com/css/css_grid.asp)

### Grid-Container

Ein Grid-Layout besteht aus einem Layout-Container und mehreren Layout-Items. Als Layout-Container kann jedes Block-Element verwendet werden, das mit `display: grid;` oder `display: inline-grid;` zu einem Grid-Container deklariert wird.

[Mehr über Grid-Container lernen...](https://www.w3schools.com/css/css_grid_container.asp)

**Template**

Um ein Grid zu definieren, stellt CSS zwei Container-Eigenschaften zur Verfügung. Mit `grid-template-columns` kann ein Raster aus Spalten angelegt werden. Dabei können mehrere Werte abstandgetrennt zugewiesen werden. Jeder Wert beschreibt die Breite einer Spalte oder teilt den freien Raum mit `auto` gleichmässig auf.

**Gutters**

`grid-gap` definiert den Abstand zwischen Spalten und Zeilen. Dieser lässst sich auch einzeln jeweils für die Spalten mit `column-gap` oder für die Zeilen mit `row-gap` deklarieren. `grid-gap` ist die Kurzform für beide und kann auch auf [Flexbox-Container] angewendet werden. Verwenden Sie immer `grid-gap` anstelle von `margin`, auch wenn Sie noch viele alte Beispiele finden werden.

**Beispiel**

~~~css
section {
    display: grid;
    grid-template-columns: 20% auto auto; /* 3 Spalten */
    grid-gap: 1em;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/GROdrBK?editors=1100)

### Grid-Items

In einem Grid-Container gelten alle **direkt** folgenden Elemente als Grid-Items. Sie werden an dem vom Container definierten Grid ausgerichtet. Ohne eine zusätzliche Deklaration wird jedes Element auf eine Spalte verteilt. Ist eine Zeile voll, wird die nächste Zeile befüllt. Grid-Items können aber auch über mehrere Spalten, Zeilen und ganze Bereiche laufen.

[Mehr über Grid-Items lernen...](https://www.w3schools.com/css/css_grid_item.asp)

**Laufweite**

`grid-column-start` beschreibt **auf welcher Spalte** ein Element platziert werden soll. Als Wert wird eine `Ganzzahl` eingesetzt. Beginnt das Element z.B. auf der zweiten Spalte, wird dies mit `grid-column-start: 2;` deklariert.

`grid-column-end` beschreibt **bis vor welche Spalte** ein Element platziert werden soll. Als Wert wird eine `Ganzzahl` eingesetzt. Endet das Element z.B. auf der dritten Spalte, wird dies mit `grid-column-end: 4;` deklariert.

**Laufhöhe**

`grid-row-start` beschreibt **auf welcher Zeile** ein Element platziert werden soll. Als Wert wird eine `Ganzzahl` eingesetzt. Beginnt das Element z.B. auf der ersten Zeile, wird dies mit `grid-row-start: 1;` deklariert.

`grid-row-end` beschreibt **bis vor welche Zeile** ein Element platziert werden soll. Als Wert wird eine `Ganzzahl` eingesetzt. Endet das Element z.B. auf der zweiten Zeile, wird dies mit `grid-row-end: 3;` deklariert.

**Kurzform**

Die Eigenschaft `grid-column` ist eine Kurzform für `grid-column-start` und `grid-column-end` und erlaubt zwei Schreibweisen. Während `grid-column: 2 / 4;` immer noch das Beispiel von oben beschreibt, kann das selbe Verhalten auch mit `grid-column: 2 / span 2;` deklariert werden. Für das Beispiel der Zeile wäre `grid-row: 1 / span 2;` entsprechend eine mögliche Kurzform.

**Beispiel**

~~~css
section {
    display: grid;
    grid-template-columns: 20% auto auto; /* 3 Spalten */
    grid-gap: 1em;
    padding: 1em;
}

div {
    background-color: PaleTurquoise;
    text-align: center;
    font-size: 2rem;
    padding: 1em;
}

div.wide {
    grid-column: 2 / span 2;
}

div.full {
    grid-column: 1 / 4;
}

div.high {
    grid-row: 1 / span 2;
}
~~~

[Beispiel mit CodePen testen](https://codepen.io/tbz-m293/pen/zYPjwZr?editors=1100)



## Checkpoints

- [ ] Ich kann erläutern wozu ein CSS-Layout dient.
- [ ] Ich kann erklären wie ein Layout aufgebaut ist.
- [ ] Ich kann die Style-Eigenschaften für Flow, Positionierung und Floating nennen.
- [ ] Ich kann das Verhalten einer Flexbox steuern.
- [ ] Ich kann ein Layout mit Grid-System umsetzen.



## Ressourcen

1. [Cascading Style Sheets](https://de.wikipedia.org/wiki/Cascading_Style_Sheets) (Wikipedia)
2. [CSS-Tutorial](https://www.w3schools.com/css/default.asp) (W3Schools)
3. [CSS first steps](https://developer.mozilla.org/de/docs/Web/CSS) (Mozilla Developer Web Docs)
4. [CSS lernen und anwenden](https://www.w3.org/Style/CSS/learning) (W3C Cascading Style Sheets)
5. [CSS Layout Patterns](https://csslayout.io/) (Sammlung von Gestaltungsmustern)
6. [CSS Layout Workshop](https://www.youtube.com/watch?v=yMEjLBKyvEg) (Chrome Dev Summit Video)



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 12.05.2022, Florian Huber
