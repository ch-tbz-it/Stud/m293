# Kompetenzen

![TBZ Logo](../x_gitressources/tbz_logo.png)

Das Modul wird als SOL-Modul durchgeführt. Als Basis dient das folgende Kompetenzraster

![Kompetenzraster](./x_gitressources/Kompetenzraster.png)

Hinweise:

- Die Farben geben einen Hinweis auf die Gruppenform. Schauen sie sich die Legende an
- Die Sterne bezeichnen die Schwierigkeit der Kompetenz.

**Leistungsmessung**:

- Es wird am Schluss ein Gesamtprojekt gewertet, welches sich über die grünen Bereiche erstreckt.
- Der Ecolm-Test zählt als Teil des Gesamten. Die Anzahl Punkte geteilt durch alle möglichen Punkte.

**Arbeitsweise:**

1. Sie arbeiten sich in zweier-Gruppen durch die Bereiche (in genau dieser Reihenfolge) T1 > H1 > D1 > T2 > H2 > D2 und lernen den Inhalt und machen dazu die Übungen.
2. Nach dem sie Punkt 1 erledigt haben und alle Übungen abgegeben haben, können sie mit dem [Projekt beginnen](Teil_Grundaufbau.md).
3. T3 müssen sie vor Projektstart bestanden haben.
4. Nachdem die LP jeweils das vorherige Teilprojekt abgenommen hat (zuerst den Grundaufbau gem. Punkt 2) können sie folgende Teilprojekte bearbeiten:
   1. **Wichtig**: Sie bearbeiten zuerst den Lerninhalt, dann die Übungen und erst dann starten sie mit dem Projektteil. Jeden Teil lassen sie sich von der Lehrperson bestätigen bevor sie zum nächsten übergehen.
   2. [Formulare](Teil_Formulare.md)
   3. [Responsive Design](Teil_Responsive.md)
   4. [Optimierung](Teil_Optimierung.md)
   5. [Media](Teil_Medien.md)
   6. [Advanced CSS](Teil_Preprocessor.md)

---

&copy;TBZ, 2022, Modul: m293
