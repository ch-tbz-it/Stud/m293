![TBZ Logo](../x_gitressources/tbz_logo.png)

# Optimierung

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

**Kopieren sie ihre Webseite aus dem vorherigen Teilprojekt in den Ordner 'Optimierung' bevor sie Änderungen beginnen**

**Publizieren sie ihre Webseite bevor Sie sie abgeben.**

#### a) Komprimierung/Minimierung

Verwenden sie *html-minifier* und *css-minify* um ihre Inhalte zu minimieren. Erstellen sie einen Task Runner (z.B. Node Script) um:

1. Die bestehenden Ziel-Dateien zuerst zu löschen (rifraf ...)
2. Die Inhalte zu minimieren.
3. Die Bilder und andere Ressourcen zu kopieren.

Zeigen sie der Lehrperson, dass sie den Task Runner in der Console aufrufen können und dieser auch korrekt funktioniert.

#### b) Automatisieren sie ihre Publikation

Bisher hatten sie ihre Inhalte manuell hochgeladen. Automatisieren sie dies, indem sie ein Skript schreiben. Informationen dazu finden sie im Kapitel T2. 

Verwenden sie ausserdem den Task Runner aus Kapitel a) um ihre FTP-Skript aufzurufen.

---

&copy;TBZ, 2022, Modul: m293