![TBZ Logo](../x_gitressources/tbz_logo.png)

# Responsive Design

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

**Kopieren sie ihre Webseite aus Teilprojekt 'Formulare' in den Ordner 'Responsive' bevor sie Änderungen beginnen**

**Publizieren sie ihre Webseite bevor Sie sie abgeben.**

#### a) Styleguide Update

Sie haben nun eine Seite, die für Desktops gut aussieht. Sie möchten nun aber auch die Smartphone-Ansicht unterstützen. Definieren sie Break-Points in ihrem Styleguide. Es reicht, wenn sie einen Breakpoint für Smartphones erstellen. Sie dürfen aber auch gerne weitere verwenden. 

Das Schriftbild und Abstände und auch Wireframes für die Smartphone-Sicht sind **optional im Styleguide** .

#### b) Responsive Site erstellen

Wenn sie nicht schon die Inhalte im Styleguide definiert haben, überlegen sie sich hier, wie ihre Seiten auf dem Smartphone aussehen sollen. Verwenden sie **keine** Frameworks wie Bootstrap.

Ein paar Möglichkeiten:

- Evtl. muss die Schriftgrösse angepasst werden.
- Abstände zwischen Elementen benötigen evtl. ebenfalls Anpassungen
- Formularlabels und -Felder können nicht mehr nebeneinander stehen. Erweitern sie das CSS, so dass die Label und Felder (beim Break-Point) übereinander liegen.
- Verkleinern sie Bilder/Logos
- Platzieren sie ihre Navigation in ein Burger-Menu. Eine Anleitungen finden sie [unter diesem Link](https://alvarotrigo.com/blog/hamburger-menu-css-responsive/), die komplett mit CSS auskommt (also ohne JavaScript)

---

&copy;TBZ, 2022, Modul: m293