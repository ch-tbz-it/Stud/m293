![TBZ Logo](../x_gitressources/tbz_logo.png)

# Medien

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

**Kopieren sie ihre Webseite aus dem vorherigen Teilprojekt in den Ordner 'Preprocessor' bevor sie Änderungen beginnen**

**Publizieren sie ihre Webseite bevor Sie sie abgeben.**

#### a) Von SASS/LESS/SCSS nach CSS

Verwenden sie den Preprocessor ihrer Wahl und bilden sie ihr CSS ab. SCSS und LESS sind ähnlich und einfacher zu lernen als SASS. Dafür hat SASS mehr Funktionalität wie z.B. Loops. Stellen sie sicher, dass

- ... sie sinnvolle Inhalte in Variablen auslagern, z.B. Farben
- ... sie sinnvolle Inhalte in Mixins auslager, z.B. Schriftbild
- ... sie Verschachtelungen von Anweisungen anwenden
- ... weiteres ...

#### b) Automatisierung

Ihr Node Script aus dem Teil "Optimierung" soll nun auch ihre Preprocessor-Datei umwandeln in CSS bevor optimiert wird.

---

&copy;TBZ, 2022, Modul: m293
