![TBZ Logo](../x_gitressources/tbz_logo.png)

# Medien

Falls sie bei den einzelnen Punkten fragen haben, gehen sie direkt zu der Lehrperson. Sie kann ihnen Beispiele zeigen und eine vertiefte Beschreibung liefern. 

**Kopieren sie ihre Webseite aus dem vorherigen Teilprojekt in den Ordner 'Medien' bevor sie Änderungen beginnen**

**Publizieren sie ihre Webseite bevor Sie sie abgeben.**

#### a) Werbe- oder Produktvideo

Produzieren sie ein Werbevideo für ihre Webseite. Sie können das Video einfach mit ihrem Smartphone oder Kamera aufnehmen. Wenn sie möchten, dürfen sie auch gerne Plattformen verwenden, um Videos zu erstellen/schneiden. Es gibt z.B. Platformen für Comic-Videos.

Konvertieren sie das Video in die notwendigen Formate (mindestens mp4 und webm) und fügen sie es auf Ihrer Webseite ein.

Achten sie darauf, dass ihr Video auch responsive sein muss, also auch auf Smartphones sinnvoll dargestellt wird (normalerweise 100% Breite).

Fügen sie das Video auf ihrer Startseite ein. 

#### b) Bilder semantisch aufarbeiten

Gehen sie durch alle ihre Seiten und verwenden sie die korrekten Tags für die Einbindung von Bildern. Entscheiden sie sich, ob an den entsprechenden Positionen der *picture*-Tag sinnvoller wäre (z.B. Header-Bilder). Fügen sie den *figure*- und *figcaption*-Tag ein, falls notwendig.

---

&copy;TBZ, 2022, Modul: m293
