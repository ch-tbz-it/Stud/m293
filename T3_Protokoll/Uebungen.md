# T3 Übungen



### Übung 1 - Requests und Responses analysieren

Öffnen sie eine beliebige Seite (z.B. <https://www.tbz.ch>) und verwenden sie die DevTools (im Netzwerk-Reiter), um die verschiedenen Requests zu analysieren. Sie können die verschiedenen Request-Typen (z.B. Dokument, JS, CSS, etc) auch filtern.

Schauen sie sich von verschiedenen Typen, sowohl die Requests als auch die Responses und dann jeweils den Header und den Body. Forschen sie nach, wenn etwas unklar ist.

Vergleichen sie die Header, speziell die Inhaltstypen.



### Übung 2 - Port

Ändern sie den Port einer URL manuell. Versuchen sie die Standard-Ports zusätzlich anzugeben. Anschliessend versuchen sie einen anderen beliebigen Port. Was geschieht?



### Übung 3 - Anker

Auf dieser Gitlab-Seite gibt es viele Anker. Schaffen sie es die URL so zu ändern, dass sie direkt zu den Übungen springen?

**TIPP**: Die Anker sind in den Titel gesetzt.



### Übung 4 - POST vs GET

Rufen sie [dieses Formular](https://ch-tbz-it.gitlab.io/Stud/m293g/m293assets/T2_Protokoll/formular.html) auf und starten sie die DevTools. Tätigen sie folgende Einstellungen

![Settings](x_gitressources/Uebung4-Einstellungen.png)

1. Laden sie nun die Seite neu
2. Schicken sie das Formular ab, damit sie einen zweiten Eintrag im Netzwerk-Reiter sehen.

Tasks:

1. Analysieren sie die beiden Requests. Ein GET-Request, welcher das Formular liest. Ein POST-Request, welcher die Formular-Daten an den Server schickt und eine Antwort erhält.
2. Was ist die Antwort im Post-Request?
3. Drücken sie nun F5, was die Seite neu lädt. Sie kriegen eine Bestätigungsnachricht. Was bedeutet diese?
4. Was bedeutet die Einstellung "Preserve log", die wir in den DevTools gemacht haben? Testen sie den Unterschied.
5. Was bedeutet die Einstellung "Doc", die wir in den DevTools (unter Filter) gemacht haben? Testen sie den Unterschied und aktivieren sie wieder "All".