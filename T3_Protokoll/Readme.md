![TBZ-Logo](../x_gitressources/tbz_logo.png)

# HTTP-Protokoll (T3)

TBZ Informatik Modul 293 - Webauftritt erstellen und veröffentlichen

[TOC]

## Lernziele

- Ich kann erläutern für was die Abkürzung HTTP steht und wofür es entwickelt wurde.
- Ich kann die Bestandteile einer URL zerlegen und benennen.
- Ich kann einen Seiten-Aufruf mit Request und Response erklären.
- Ich kann die Bestandteile einer HTTP-Anfrage benennen.
- Ich kann die Bestandteile einer HTTP-Antwort benennen.
- Ich kann fünf verschiedene Übertragungs-Methoden nennen und den Unterschied von GET und POST erklären.
- Ich kann die Standard-Ports eines Webservers nennen.
- Ich kann die wichtigsten Status Codes erklären.
- Ich kann den korrekten Content-Type von Requests und Responses evaluieren und setzen.



## Uniform Resource Locator (URL)

Eine URL dient dazu eine Ressource eindeutig zu kennzeichnen und adressieren. Clients können so spezifische Dokument anfragen. Die allgemein Form sieht wie folgt aus

![url_scheme](x_gitressources/URL.png)

**Schema**: Das Schema ist immer notwendig und kennzeichnet das Protokoll. Normalerweise sehen sie hier *http* und *https*, aber auch *mailto*, *ftp* und andere sind möglich. Nach dem Protokoll kommt die Zeichenfolge "&#58; &#47; &#47;" (ohne Lehrzeichen), die das Protokoll von der Domäne trennt.

**Domäne**: Anstelle der Domäne kann auch eine IP stehen, da die Domäne ja sowieso nur ein Alias für eine IP ist.

**Port**: Der Port wird normalerweise nicht angegeben und ist standardmässig 80 für *http* und 443 für *https*. Es ist auch möglich, dass ein Webserver einen anderen Port verwenden (z.B oft 8080). Dann muss der Port zwingend angegeben werden.

**Pfad**: Alle Dateien werden in einer Ordner-Struktur abgelegt. Mit dem Pfad navigieren sie zu der entsprechenden Datei. HTML-Dateien haben die Dateierweiterungen *.html* oder *.htm*. Andere sind möglich, speziell auch für dynamische Seiten. Wenn der Dateiname beim Pfad weggelassen wird, liefert der Webserver automatisch die Datei mit dem Namen *index.html* aus. Bei dynamischen Applikationen stimmt der Pfad in der URL oft nicht überein mit der Dateiablage auf dem Server.

**Argumente**: Argumente sind ein Weg Daten an den Server zu übermitteln. Der Server muss aber so eingerichtet sein, dass die Daten auch empfangen werden. Wenn dies nicht der Fall ist, werden die Argumente einfach ignoriert. Argumente werden immer in Schlüssel-Werte-Paaren übertragen, wobei diese mit dem kaufmännischen UND (*&*) getrennt werden. Das Fragezeichen (*?*) kennzeichnet den Start der Argumente.

**Anker**: Ein Anker wird mit dem *a*-Tag gesetzt. In diesem Fall dient dieser Tag nicht als Link, sondern als Anker. Wenn der Anker (mit dem korrekten Name) in der URL angehängt wird, springt die Seite direkt zu diesem Anker. Einem Anker wird immer das Zeichen *#* vorangestellt.



## Geschichte von HTTP

Die Entwicklung des **Hypertext Transfer Protocols** (HTTP), hängt eng mit der Entwicklung der Hypertext Markup Language (HTML) zusammen. HTTP bezeichnet das Protokoll, während HTML das Dokument - also den Inhalt - definiert.

- 1989: HTTP wird zusammen mit HTML und dem Konzept von URLs entwickelt.
- 1996: HTTP/1.0 wird publiziert. Die Schwachstelle hier ist, dass jedes Objekt, also auch eingebettete Bilder eine neue Verbindung benötigt. Ein Verbindungsaufbau ist grundsätzlich langsam.
- 1999: HTTP/1.1 wurde zusammen mit HTML 4.01 eingeführt und löst das Problem mit den vielen Verbindungen. Bis anhin konnten Daten nur empfangen werden, aber mit der Einführung dieser Version konnten Daten nun auch gesendet werden.
- 2015: HTTP/2 hat Möglichkeit mehrere Anfragen zusammenzufassen, so dass die Antwortzeiten viel kürzer wurden. Zusätzliche Erweiterungen sind Datenkompression und Push-Verfahren (Server-initiierte Datenübertragungen).
- 2018: HTTP/3. An dieser Version wird gearbeitet und sie ist noch nicht veröffentlicht.



## Wie funktioniert HTTP?

### TCP-Kommunikation

HTTP ist auf TCP aufgebaut und verwendet ein Client-Server Kommunikationsmodell mit Anfragen (Requests) und Antworten (Response). Jeder Request und jede Response sendet Nachrichten mit einem Kopf- und einen Inhaltsteil (Header und Body).

![httpmessage](x_gitressources/RequestResponse.png)

### HTTP-Kommunikation

Während auf der TCP-Schicht ganz allgemein Nachrichten (*Messages*) geschickt werden, definiert die Applikationsschicht (HTTP) welche Inhalte die Nachrichten haben.

Im Header der Nachricht (grün) werden Protokoll-Informationen und der HTTP Header verschickt, wie in folgendem Beispiel gezeigt wird. Der Nachrichten-Header (TCP-Schicht) entspricht also nicht dem HTTP-Header (Applikationsschicht), aber der HTTP-Header ist ein Teil des Nachrichten-Headers.

![Example](x_gitressources/RequestResponseExample.png)

Folgende Tabellen zeigen die Bestandteile der Requests und Responses.

### HTTP-Request

| Bezeichnung | Beschreibung |
| ------------ | ------------------------------------------------------------------------- |
| Request-Informationen | Hier finden sie drei Bestandteile<br />- Die Methode (dazu später mehr)<br />- Den Pfad<br />- Das verwendete Protokoll |
| Request-Headers | Verschiedene Headers, die als Key-Value-Paare übertragen werden (dazu später mehr) |
| Request-Body | Den Inhalt, der dem Server übertragen wird (dazu später mehr). Nicht jeder Request hat Inhalt in seinem Body (er kann also leer sein) |

### HTTP-Response

| Bezeichnung | Beschreibung |
| ------------ | ------------------------------------------------------------------------- |
| Response-Informationen | Hier finden sie drei Bestandteile<br />- Das verwendete Protokoll<br />- Den Status Code<br />- Die Status-Nachricht |
| Response-Headers | Verschiedene Headers, die als Key-Value-Paare übertragen werden (dazu später mehr) |
| Response-Body | Den Inhalt, der zu dem Client übertragen wird, meistens HTML, JavaScript und CSS Inhalte. |

#### Status Code

Wie sie eben gesehen haben, sendet jeder Response einen **Status-Code**, der aussagt, ob die Anfrage erfolgreich war und wenn nicht, welcher Fehler aufgetreten ist. Jeder Status-Code wird mit einer **Status-Nachricht** zurückgeschickt. Die wichtigsten Status-Codes sind:

- **200 OK**: Die Anfrage war erfolgreich
- **404 Not Found**: Die Ressource wurde nicht gefunden. Evtl. wurde ein falscher Pfad mitgeliefert.
- **500 Internal Server Error**: Dieser Fehler kann auftreten bei dynamischen Seiten, die Fehler werfen.

[Komplette Liste](https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html) der Status-Codes wie sie von W3C definiert sind.



## Headers

Es existieren viele offizielle Headers für Request und Response und auch viele, die offiziell nicht existieren, aber trotzdem Verwendung finden. Alle nicht-offiziellen beginnen mit "*X-*" (z.B. x-forwarded-for) und werden für proprietäre Verhalten oder Inhalte verwendet.

### Request Headers

Die wichtigsten und typischen Request-Headers im Überblick

- accept: Eine komma-separierte Liste mit Inhalten, die der Client akzeptieren wird, z.B. "*text/html;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9*"
- accept-encoding: Codierung und Verschlüsselung, z.B. "*gzip, deflate, br*"
- accept-language: Liste von Sprachen, z.B. "*en-US,en;q=0.9,de;q=0.8*"
- cache-control:  Caching (Zwischenspeicher)-Regeln, z.B. "*max-age=0*"
- content-length: Länge des Inhalts in bytes, falls einer mitgeschickt wird, z.B. "*34*"
- content-type: Typ des Inhalts, falls einer mitgeschickt wird, z.B. "*application/x-www-form-urlencoded*"
- cookie: Alle cookies, die gesetzt sind, z.B. "*_ga=GA1.2.699450697.1631599704; _gid=GA1.2.719813276.1640619849; _gat=1*"
- Referer: Welche URL auf diese Seite weitergeleitet hat, falls weitergeleitet wurde, z.B. "*<https://www.tbz.ch>*"
- user-agent: Eine Zeichenkette, die den Client identifiziert, z.B. "*Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36*"

### Response Headers

Die wichtigsten Response-Headers im Überblick

- cache-control: Anweisung an den Client/Browser wie die Seite mit dem Cache umgehen soll, z.B. "*no-cache, no-store, max-age=0, must-revalidate*"
- content-encoding: Kompressionstyp, der verwendet wurde, z.B. "*br*"
- content-type: Inhaltstyp und Textcodierung, der geschickt wird, z.B. "*text/html; charset=UTF-8*"
- date: Datum der Antwort, z.B. "*Mon, 27 Dec 2021 15:44:21 GMT*"
- expires: Datum an dem der Inhalt nicht mehr gültig ist. Wird oft auf das gleiche Datum gesetzt, wie der Header *date*, damit nichts im Cache gespeichert wird, z.B. "*Mon, 27 Dec 2021 15:44:21 GMT*"



## Request-Methoden

HTTP definiert ein Set von Methoden (auch oft als *verb* bezeichnet), die bei Anfragen verwendet werden. Die Methoden geben einen Hinweis darauf, was eine Anfrage erreichen möchte. Einige Methoden haben Einschränkungen, die man nicht umgehen kann, aber andere können einfach ausgetauscht werden, wobei dann die semantische Bedeutung nicht mehr mit dem Request übereinstimmt.

Gesteuert werden die Methoden auf der Seite des Servers. Der Client kann zwar jede URL, mit einer beliebigen Methode aufrufen, aber der Server wird nicht auf alle Methoden antworten.

Im früheren Beispiel, sehen sie, dass die Methode als Teil des Request-Headers mitgeschickt wird.

Die wichtigsten Methoden:

- GET: Bei dieser Methode können Daten **nur** in der URL übermittelt werden. Der Request-Body ist leer.
- POST: Diese Methode sendet Daten an den Server, wobei typischerweise der Request-Body verwendet wird. Es gibt verschiedene Inhaltstypen, die später besprochen werden. Wenn sie sich unsicher sind, welche Methode sie verwenden sollen, benutzen sie normalerweise diese.
- PUT: Wird verwendet, wenn sie ein Objekt erstellen oder aktualisieren möchten, z.B. ein Produkt-Eintrag. Auch hier wird typischerweise der Request-Body verwendet. Der Unterschied zu der Methode POST ist hauptsächlich semantisch. Wenn PUT mehrmals aufgerufen wird (mit den gleichem Inhalt und Parameter) sollten keine Nebeneffekte auftreten, z.B. mehrere Objekte werden angelegt, da PUT das Objekte einfach aktualisieren sollte. Der server-seitige Code muss natürlich diese Semantik auch korrekt umsetzen.
- DELETE: Wird verwendet, wenn sie ein Objekt löschen möchten, z.B. ein Produkt-Eintrag. Der Request-Body ist leer, der Response-Body kann einen Inhalt haben.
- OPTIONS: Wird verwendet, wenn sie herausfinden möchten, welche Methoden diese URL unterstützt. Der Request- und auch der Response-Body sind leer.
- HEAD: Wird verwendet, wenn sie nur die Header Informationen möchten. Der Request- und auch der Response-Body sind leer.
- Weitere: CONNECT, TRACE, PATCH

Wenn sie **statische Webseiten** erstellen, werden sie **nur die Methoden *GET* und *POST* verwenden**. Als Grundregel soll gelten, dass Formulare mit *POST* geschickt werden und andere Anfragen via *GET* Daten gelesen werden.

[Details zu den Methoden](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)

## Inhaltstypen (Content-Type)

Wir haben bereits gelernt, dass der Inhalt im Request- oder Response-Body übertragen werden. Einige Inhaltstypen (auch Mime-Types oder Content-Types genannt) werden bei Requests und andere bei Responses verwendet. Einige werden folgend vorgestellt, andere werden vertieft behandelt, wenn sie etwas über Formulare lernen.

| Content-Type | Beschreibung |
| --------------------------- | ------------------------------------------------------------------------------------------ |
| text/html | Dieser Typ sehen sie (fast) nur bei Responses und ist der typische Content-Type, wenn sie eine Webseite erhalten.<br />**Beispiel**: &lt;html&gt;&lt;body&gt;Hello World&lt;/body&gt;&lt;/html&gt;  |
| application/x-www-form-urlencoded | Dieser Typ wird verwendet, wenn sie ein Formular mit der Methode *POST* senden. |
| multipart/form-data | Dieser Typ wird verwendet, wenn sie ein Formular mit der Methode *POST* senden und auch Dateien mitschicken möchten |
| application/json | JSON Daten werden übermittelt. Dieser Typ wird oft verwendet, bei APIs  |
| application/xml | XML Daten werden übermittelt. Dieser Typ wird oft verwendet, bei APIs |
| application/javascript | JavaScript-Daten werden übermittelt. Normalerweise kommt dieser Typ nur bei Responses vor und nicht bei Requests. |
| application/image | Bilder werden übermittelt. Normalerweise kommt dieser Typ nur bei Responses vor und nicht bei Requests. |
| text/css | Stylesheet-Daten werden übermittelt. Normalerweise kommt dieser Typ nur bei Responses vor und nicht bei Requests. |



## Hypertext Transfer Protocol Secure (HTTPS)

HTTPS basiert auf HTTP und erweitert das Protokoll um eine Sicherheitsschicht, die es erlaubt Daten mit Verschlüsselung (TLS und SSL) zu übertragen.

Aktuell verwendet werden die Verfahren *TLS 1.2* und *TLS 1.3*, welche bisher ohne Schwachstellen sind.



## Checkpoints

- [ ] Ich kann erläutern für was die Abkürzung HTTP steht und wofür es entwickelt wurde.
- [ ] Ich kann die Bestandteile einer URL zerlegen und benennen.
- [ ] Ich kann einen Seiten-Aufruf mit Request und Response erklären.
- [ ] Ich kann die Bestandteile einer HTTP-Anfrage benennen.
- [ ] Ich kann die Bestandteile einer HTTP-Antwort benennen.
- [ ] Ich kann fünf verschiedene Übertragungs-Methoden nennen und den Unterschied von GET und POST erklären.
- [ ] Ich kann die Standard-Ports eines Webservers nennen.
- [ ] Ich kann die wichtigsten Status Codes erklären.
- [ ] Ich kann den korrekten Content-Type von Requests und Responses evaluieren und setzen.



## Ressourcen

- <https://www.w3.org/Protocols/>
- <https://developer.mozilla.org/en-US/docs/Web/HTTP>
- <https://developer.mozilla.org/en-US/docs/Learn/Common_questions/What_is_a_URL>



**Informatik Modul 293 - Webauftritt erstellen und veröffentlichen**

Letzte Aktualisierung: 15.05.2022, Yves Nussle
