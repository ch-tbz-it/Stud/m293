## T1 Aufträge 

### Auftrag 1 - Visual Studio Code installieren

1. Installieren sie VSC. Die Anleitung finden sie im Script.
2. Erstellen sie einen neuen (leeren) Ordern.
3. Erstellen sie eine neue Datei *index.html*
4. Kopieren sie das untenstehende HTML in diese Datei.
5. Wechseln sie in den Explorer und Doppelklicken sie die Datei. Sie sollten nun Hello World sehen.

~~~html
<!DOCTYPE html>
<html>
    <head></head>
    <body>
        <h1>Hello World</h1>
    </body>
</html>
~~~

### Auftrag 2 - Erweiterung/Extension installieren

1. Installieren sie eine der beiden oder auch beide Extensions nach der Anleitung oben
   1. HTML Preview
   2. Live Server
2. Schauen sie sich die - in Auftrag 1 - erstellte Datei mit der Erweiterung an.



### Auftrag 3 - Gitlab oder Github einrichten

Erstellen sie das GIT-Repository für dieses Modul. Sie hatten die Grundlagen dazu bereits im [Modul 231](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/02_git). Ihr Repository ist entweder public oder sie laden die Lehrperson als Collaborator ein. 